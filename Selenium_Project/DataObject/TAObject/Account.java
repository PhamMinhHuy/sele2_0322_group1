package TAObject;

public class Account {
	private String id;
	private String password;
	private String pID;
	
	public Account(String id, String password) {
		this.id = id;
		this.password = password;
	}
	
	public Account(String id, String password, String pID) {
		this.id = id;
		this.password = password;
		this.pID = pID;
	}

	
	public void setid(String id) {
		this.id = id;
	}
	
	public void setPassword(String password) {
		this.password = password;
	}
	
	public void setPID(String pID) {
		this.pID = pID;
	}
	
	public String getid() {
		return this.id;
	}
	
	public String getPassword() {
		return this.password;
	}
	
	public String getPID() {
		return this.pID;
	}
}
