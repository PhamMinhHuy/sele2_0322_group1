package TADashboard;


import common.Constant;
import common.Utilities;
import ElementWrapper.Element;
import TAObject.Account;

public class LoginPage extends GeneralPage{
	private final Element txtUserName = new Element("//input[@id='username']");
	private final Element txtPassWord = new Element("//input[@id='password']");
	private final Element loginBtn = new Element("//div[@class='btn-login']");
	private final Element repositoryChk = new Element("//select[@id='repository']");
	
	public LoginPage enterUserName(String sUserName) {
		this.txtUserName.clickVisibleElement(Constant.TIMEOUT);
		this.txtUserName.sendKeys(sUserName);
		return this;
	}
	
	public LoginPage enterPassWord(String sPassWord) {
		this.txtPassWord.clickVisibleElement(Constant.TIMEOUT);
		this.txtPassWord.sendKeys(sPassWord);
		return this;
	}
	
//	public void login(Account account) {
//		enterUserName(account.getid());
//		enterPassWord(account.getPassword());
//		this.loginBtn.clickVisibleElement(Constant.TIMEOUT);
//	}
	public MainPage login (Account account) {
		Utilities.waitForPageLoading();
		this.selectRepository(Constant.REPOSITORY_NAME_2);
		enterUserName(account.getid());
		enterPassWord(account.getPassword());
		loginBtn.clickVisibleElement(Constant.TIMEOUT);
		return new MainPage();
	}

	public void loginWithOutAccount() {
		this.loginBtn.clickVisibleElement(Constant.TIMEOUT);
	}
	
	public void selectRepository(String sRepositoryName) {
		this.repositoryChk.selectItemInDropDownList(sRepositoryName, Constant.TIMEOUT);
	}
	
}
