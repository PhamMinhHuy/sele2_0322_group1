package TADashboard;

import ElementWrapper.Element;
import TAPopup.AddPagePopup;
import common.Constant;
import common.Utilities;
import driverWrapper.DriverManager;

public class GeneralPage {
	private final Element btnOverView = new Element("//a[text()='Overview']");
	private final Element btnDashBoard = new Element("//a[contains(text(),'Dashboard')]");
	
	private final String sPageNew = "//a[text()='%s']"; 
	private final String sChildPage = "//a[text()='%s']//following-sibling::ul[//a[text()='%s']]"; 
	private final String sNewPageBesidesOldPage = "//li[./a[text()='%s']]//preceding-sibling::li[./a[text()='%s']]";
	private  final Element loadingImg = new Element("//div/img[contains(@src,'loading')]");
	private final Element CancelBtn = new Element("//input[@id='Cancel']");
	private final Element OkBtn = new Element("//input[@id='OK']");
	private final Element finishBtn = new Element("//input[@value='Finish']");
	
	private final Element dynamicTabMenu(String path) {return new Element("//div[@id='header']//li//a[contains(@href,'"+path+"')]");}
	private final Element dynamicSubTabMenu(String path) {return new Element("//div[@id='header']//li//a[contains(text(),'"+path+"')]");}
	private final Element repositoryDiv = new Element("//div[@id='header']//li//a[contains(@href,'Repository')]/span");
	private final Element dynamicRadioBtn(String path) {return new Element("//label[contains(text(),'"+path+"')]//input[@type='radio']");}
	private final Element dynamicPageMenu(String path) {return new Element("//div[@id='container']//a[normalize-space(text())='"+path+"']");}
	private final Element dynamicCheckBoxBtn(String path) {return new Element("//label[contains(text(),'"+path+"')]//input[@type='checkbox']");}
	
	private final Element btnGlobalSetting = new Element("//li[@class='mn-setting']"); 
	private final Element btnAddPage = new Element("//a[text()='Add Page']"); 
	private final Element btnCreateProfile = new Element("//a[text()='Create Profile']"); 
	private final Element btnCreatePanel = new Element("//a[text()='Create Panel']"); 
	
	private final Element btnEdit = new Element("//a[text()='Edit']"); 
	private final Element btnDelete = new Element("//a[text()='Delete']"); 
	
	private final Element btnUser = new Element("//a[contains(@href, 'Welcome')]");
	private final Element btnMyProfile = new Element("//a[text()='My Profile']");
	private final Element btnLogout = new Element("//a[text()='Logout']");
	
//	public void waitForPageLoading() {
//		DriverManager.waitForPageLoad();
//	}
	
	public boolean isLoginSucess(String userName) {

		this.dynamicTabMenu("Welcome").waitForVisibility(Constant.TIMEOUT);

		String userAccount = this.dynamicTabMenu("Welcome").getText();

		if (userAccount.equals(userName)) {

			return true;			

		}else {

			return false;			

		}
	}
	public void selectRepositoryOnMainPage(String sRepositoryName) {

		this.selectMenu("Repository", sRepositoryName);
	}
	public String getCurrentRepositoryNameOnMainPage() {

		this.repositoryDiv.waitForVisibility();

		return this.repositoryDiv.getText();
	}
	public void goToAdministerPanelPage() {
		this.selectMenu("Administer","Panels");
	}
	
	public void goToAdministerProfilesPage() {
		this.selectMenu("Administer","Data Profiles");
	}	
	
	public void selectMenu(String Menu, String SubMenu) {
		this.dynamicTabMenu(Menu).clickVisibleElement(Constant.TIMEOUT);
		this.dynamicSubTabMenu(SubMenu).clickVisibleElement(Constant.TIMEOUT);
		Utilities.waitForPageLoading();
	}

	public void selectPage(String page) {
		this.dynamicPageMenu(page).clickVisibleElement(Constant.TIMEOUT);
	}
	
	public void selectSettingMenu(String Menu) {
		Utilities.waitForPageLoading();
		this.waitForLoading(Constant.TIMEOUT);
		btnGlobalSetting.waitForClickAble(Constant.TIMEOUT);
		btnGlobalSetting.clickVisibleElement(Constant.TIMEOUT);
		Element e = new Element(this.btnGlobalSetting.getLocatorAsString() + String.format("//a[text()='%s']", Menu));
		e.waitForDropDownListPopulated(Constant.TIMEOUT);
		e.click();
//		Utilities.waitForPageLoading();
	}
	
	public void createPanel() {
		this.selectSettingMenu("Create Panel");
	}
	
	public void createProfile() {
		this.selectSettingMenu("Create Profile");
	}
	
	public void deletePage(String page) {
		this.selectPage(page);
		this.selectSettingMenu("Delete");
		Utilities.acceptAlert();
	}
	
	public void editPage(String page) {
		this.selectPage(page);
		this.selectSettingMenu("Edit");
	}
	
	public AddPagePopup openAddpagePopup() {
		Utilities.waitForPageLoading();
		btnGlobalSetting.waitForPresent();
		btnGlobalSetting.waitForClickAble(Constant.LONG_TIMEOUT);
		btnGlobalSetting.hover();
		btnAddPage.waitForDropDownListPopulated(10);
		btnAddPage.waitForClickAble();
		btnAddPage.click();
		return new AddPagePopup();
	}
	public AddPagePopup clickEditLink() {
		btnGlobalSetting.waitForClickAble();
		btnGlobalSetting.click();
		btnEdit.waitForDropDownListPopulated(10);
		btnEdit.click();
		return new AddPagePopup();
	}
	public void clickDeleteLink() {
		btnGlobalSetting.waitForClickAble();
		btnGlobalSetting.waitForPresent();
		btnGlobalSetting.click();
		btnDelete.waitForDropDownListPopulated(10);
		btnDelete.waitForClickAble();
		btnDelete.click();
	}
	public boolean isDisplayedDeleteLink() {
		btnGlobalSetting.waitForClickAble();
		btnGlobalSetting.click();
		btnDelete.waitForDropDownListPopulated(10);
		btnDelete.waitForVisibility();
		return btnDelete.isDisplayed();
	}
	
	public void hoverOnPage(String sPageName) {
		Element e = new Element(String.format(sPageNew, sPageName));
		e.waitForVisibility(Constant.LONG_TIMEOUT);
		e.hover();
	}
	
	public boolean isEnableGlobalSettingButton() {
		return btnGlobalSetting.isEnabled();
	}
	
	public void waitForMainPageLoading() {
		btnGlobalSetting.waitForVisibility(Constant.LONG_TIMEOUT);
		btnGlobalSetting.waitForClickAble();
	}
//	public LoginPage logout() {
//		btnUser.waitForClickAble();
//		btnUser.click();
//		btnLogout.waitForClickAble();
//		btnLogout.click();
//		return new LoginPage();
//	}
	
	public void logout() {
		if (this.dynamicTabMenu("Welcome").returnWaitForVisibility(Constant.TIMEOUT)) {
			this.selectMenu("Welcome", "Logout");
		}
		
	}

	public void clickOnPageLink(String sPageName) {
		Element e = new Element(String.format(sPageNew, sPageName));
		e.waitForVisibility(Constant.LONG_TIMEOUT);
		e.click();
	}
	
	public boolean isDisplayedPageNew(String sPageName) {
		Element e = new Element(String.format(sPageNew, sPageName));
		e.waitForVisibility(Constant.LONG_TIMEOUT);
		return e.isDisplayed();
	}
	public boolean isDisplayedChildPage(String sPageParentName, String sChildPageName) {
		Element e = new Element(String.format(sPageNew, sPageParentName));
		e.waitForVisibility(Constant.LONG_TIMEOUT);
		e.hover();
		e.click();
		
		Element eleChildPage = new Element(String.format(sChildPage, sPageParentName, sChildPageName));
		eleChildPage.waitForVisibility(Constant.LONG_TIMEOUT);
		return eleChildPage.isDisplayed();
	}
	
	public boolean isDisplayedPageNewBesidesOldPage(String sPageNameNew, String sPageNameOld) {
		Element e = new Element(String.format(sNewPageBesidesOldPage, sPageNameNew, sPageNameOld));
		e.waitForVisibility(Constant.LONG_TIMEOUT);
		return e.isDisplayed();
	}
	
	public void selectRadioButton(String value) {
		if (value != null) {
			this.dynamicRadioBtn(value).clickVisibleElement(Constant.LONG_TIMEOUT);
		}
	}

	public void selectCheckBox(String value) {
		if (value != null) {
			this.dynamicCheckBoxBtn(value).clickVisibleElement(Constant.LONG_TIMEOUT);
		}
	}
	
	public void waitForLoading(int timeOutInSeconds)
	{
		if (this.loadingImg.returnWaitForVisibility(timeOutInSeconds) == true) {
			this.loadingImg.returnWaitForInVisibility(timeOutInSeconds);
		}
		
	}
	public void clickOkButton() {
		if (OkBtn.returnWaitForVisibility(Constant.TIMEOUT)) {
			this.OkBtn.click();
		}
	}
	
	public void clickCancelButton() {
		if (CancelBtn.returnWaitForVisibility(Constant.TIMEOUT)) {
			this.CancelBtn.click();
		}
	}
	
	public void clickFinishButton() {
		if (finishBtn.returnWaitForVisibility(Constant.TIMEOUT)) {
			this.finishBtn.click();
		}
	}
	
	public boolean isRadioDisable(String radio) {
		if (this.dynamicRadioBtn(radio).isEnabled()) {
			return false;
		}
		return true;
	}
	
	public boolean isCheckBoxDisable(String check) {
		if (this.dynamicCheckBoxBtn(check).isEnabled()) {
			return false;
		}
		return true;
	}
}
