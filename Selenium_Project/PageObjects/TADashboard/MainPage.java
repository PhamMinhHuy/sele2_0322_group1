package TADashboard;

import ElementWrapper.Element;
import common.Constant;
import common.Utilities;

public class MainPage extends GeneralPage{
	
	private final Element dynamicTabMenu(String path) {return new Element("//div[@id='header']//li//a[contains(@href,'"+path+"')]");}
	private final Element dynamicSubTabMenu(String path) {return new Element("//div[@id='header']//li//a[contains(text(),'"+path+"')]");}
	private final Element repositoryDiv = new Element("//div[@id='header']//li//a[contains(@href,'Repository')]/span");
	
	public boolean isLoginSucess(String userName) {
		Utilities.waitForPageLoading();
		this.dynamicTabMenu("Welcome").waitForVisibility(Constant.TIMEOUT);
		String userAccount = this.dynamicTabMenu("Welcome").getText();
		if (userAccount.equals(userName)) {
			return true;			
		}else {
			return false;			
		}
	}
	
	public void selectRepositoryOnMainPage(String sRepositoryName) {
		Utilities.waitForPageLoading();
		if (this.dynamicTabMenu("Repository").returnWaitForVisibility(Constant.TIMEOUT) == true) {
			this.dynamicTabMenu("Repository").click();
			this.dynamicSubTabMenu(sRepositoryName).clickVisibleElement(Constant.TIMEOUT);
		}
	}
	
	public String getCurrentRepositoryNameOnMainPage() {
		Utilities.waitForPageLoading();
		this.waitForLoading(Constant.TIMEOUT);
		this.repositoryDiv.waitForVisibility();
		return this.repositoryDiv.getText();
	}
	
	
}
