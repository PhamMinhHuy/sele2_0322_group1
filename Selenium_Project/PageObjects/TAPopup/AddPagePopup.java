package TAPopup;

import ElementWrapper.Element;
import TADashboard.MainPage;
import common.Constant;

public class AddPagePopup {
	private final Element puNewPage = new Element("//div[@id='div_popup']/div[@class='pbox']"); 
	private final Element lbPopupTitle = new Element("//div[@class='pbox']//h2[text()='Edit Page']");
	
	private final Element txtPageName = new Element("//input[@id='name']");
	private final Element cbbParentPage = new Element("//select[@id='parent']");
	private final String sParentPageOption = "//select[@id='parent']/option[text()='%s']";
	
	private final Element cbbNumberColumn = new Element("//div[@class='pbox']//select[@id='columnnumber']");
	private final String sNumberColumnOption = "//div[@id='div_popup']/div[@class='pbox']//select[@id='columnnumber']/option[text()='%s']";
	
	private final Element cbbDisplayedAfter = new Element("//select[@id='afterpage']");
	private final String sDisplayedAfterOption = "//select[@id='afterpage']/option[text()='%s']";
	
	private final Element cbPulic = new Element("//input[@id='ispublic']");
	
	private final Element btnOK = new Element("//input[@id='OK']");
	private final Element btnCancel = new Element("//input[@id='Cancel']");
	
	
	public String getTextPopupTitle() {
		lbPopupTitle.waitForVisibility();
		return lbPopupTitle.getText();
	}
	public boolean isDisplayedAddPagePopup() {
		puNewPage.waitForVisibility();
		return puNewPage.isDisplayed();
	}
	public void waitForDisplayedAddPagePopup() {
		puNewPage.waitForVisibility();
	}
	public void waitForNotDisplayedAddPagePopup() {
		puNewPage.waitForElementNotDisplay();
	}
	
	public MainPage addNewChildPage(String sPageName, String sParentPagename, boolean isPulic) {
		enterPageName(sPageName);
		selectParentPage(sParentPagename);
		checkPublic(isPulic);
		clickOkButton();
		return new MainPage();
	}
	public MainPage addNewParentPage(String sPageName, boolean isPulic) {
		enterPageName(sPageName);
		checkPublic(isPulic);
		clickOkButton();
		return new MainPage();
	}
	
	public void enterPageName(String sPageName) {
		txtPageName.waitForVisibility();
		txtPageName.enter(sPageName);
	}
	
	public void selectParentPage(String sParentPageName) {
		Element e = new Element(String.format(sParentPageOption, sParentPageName));
		cbbParentPage.waitForClickAble();
		cbbParentPage.click();
		e.waitForClickAble();
		e.click();
	}
	public void selectDisplayedAfter(String sPageName) {
		Element e = new Element(String.format(sDisplayedAfterOption, sPageName));
		cbbDisplayedAfter.waitForClickAble();
		cbbDisplayedAfter.click();
		e.waitForDropDownListPopulated(Constant.SHORT_TIMEOUT);
		e.waitForClickAble();
		e.click();
	}
	
	public void checkPublic(boolean isCheck) {
		if(cbPulic.isCheck() != isCheck) {
			cbPulic.click();
		}
	}
	
	public MainPage clickOkButton () {
		btnOK.waitForClickAble();
		btnOK.click();
		waitForNotDisplayedAddPagePopup();
		return new MainPage();
	}
	public MainPage clickCancelButton () {
		btnCancel.waitForClickAble();
		btnCancel.click();
		waitForNotDisplayedAddPagePopup();
		return new MainPage();
	}
}
