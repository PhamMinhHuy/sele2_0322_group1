package TAPopup;

import ElementWrapper.Element;
import TADashboard.GeneralPage;
import common.Constant;

public class CreateProfilesPage extends GeneralPage{
	
	private final Element nameTxt = new Element("//input[@id='txtProfileName']");
	private final Element itemTypeCbb = new Element("//select[@id='cbbEntityType']");
	private final Element relatedDataCbb = new Element("//select[@id='cbbSubReport']");
	
	public void inputValueOnAddNewProfilesWindow(String name, String itemType, String relatedData) {
		//Set argument = null if don't want input that value
		this.nameTxt.inputOnVisibleElement(name, Constant.TIMEOUT);
		this.itemTypeCbb.selectItemInDropDownList(itemType, Constant.TIMEOUT);
		this.relatedDataCbb.selectItemInDropDownList(relatedData, Constant.TIMEOUT);
		this.clickFinishButton();
		this.waitForLoading(Constant.TIMEOUT);
	}
}
