package TAPopup;

import java.util.List;

import ElementWrapper.Element;
import TADashboard.GeneralPage;
import common.Constant;
import common.Logger;
import common.Utilities;

public class CreatePanelsPage extends GeneralPage{
	
	private final Element addNewDialogDiv = new Element("//div[@class='ui-dialog-container']//span[normalize-space(text())='Add New Panel']");
	private final Element displayNametxt = new Element("//input[@name='txtDisplayName']");
	private final Element seriesCbb = new Element("//select[@id='cbbSeriesField']");
	private final Element panelSettingDiv = new Element("//div[@id='tdSettings']//legend");
	private final Element dataProfileCbb = new Element("//select[@id='cbbProfile']");
	private final Element chartTitletxt = new Element("//input[@name='txtChartTitle']");
	private final Element chartTypeCbb = new Element("//select[@id='cbbChartType']");
	private final Element dynamicTextInput(String path) {return new Element("//td[contains(text(),'"+path+"')]/following-sibling::td[1]/*[1]");}


	public boolean isOtherControlerDisable() {
		this.addNewDialogDiv.waitForVisibility();
		this.logout();
		return this.addNewDialogDiv.returnWaitForVisibility(Constant.TIMEOUT);
	}
	
	public void inputValueOnAddNewPanelWindow(String displayName, String series, String type, String profile, String chartTitle) {
		//Set argument = null if don't want input that value
		this.displayNametxt.inputOnVisibleElement(displayName, Constant.TIMEOUT);
		this.dataProfileCbb.selectItemInDropDownList(profile, Constant.TIMEOUT);
		this.selectTypeAddNewPanelWindow(type);
		this.chartTitletxt.inputOnVisibleElement(chartTitle, Constant.TIMEOUT);
		this.seriesCbb.selectItemInDropDownList(series, Constant.TIMEOUT);
		this.clickOkButton();
	}
	
	public boolean isPanelSettingCorrectWithValue(String expected) {
		return this.panelSettingDiv.getText().equals(expected);
	}
	
	public void selectChartTypeAddNewPanelWindow(String type) {
		this.chartTypeCbb.waitForVisibility();
		this.chartTypeCbb.selectItemInDropDownList(type, Constant.TIMEOUT);
	}

	public void selectTypeAddNewPanelWindow(String type) {
		this.selectRadioButton(type);
		this.panelSettingDiv.waitForElementNotChange(Constant.SHORT_TIMEOUT);
	}
	
	public boolean isSelectOptionsInAlphabeticalOrder() {
		this.dataProfileCbb.waitForVisibility();
		List<String> listItem = this.dataProfileCbb.getAllItemsInDropDownList();
		List<String> listSortedItem = Utilities.sortListItem(listItem);
		this.clickCancelButton();
		return listItem.equals(listSortedItem);
	}
	
	public boolean isDataProfilesPopulatedInSelectOptions(String data) {
		this.dataProfileCbb.waitForVisibility();
		List<String> listItem = this.dataProfileCbb.getAllItemsInDropDownList();
		this.clickCancelButton();
		return listItem.contains(data);
	}
	
	public boolean isChartTypePopulatedInSelectOptions(String data) {
		this.chartTypeCbb.waitForVisibility();
		List<String> listItem = this.chartTypeCbb.getAllItemsInDropDownList();
		return listItem.contains(data);
	}

	public boolean checkInputStatus(String listData) {
		try {
	//		listData is list string include name and type of input: ex: Name,other,disable;Chart,radio,enable 
			String[] list = listData.split(";");
			for (String item : list) {
				System.out.println(item);
				String[] i = item.split(",");
				if (i.length == 3) {
	//				i[0] is name of input,i[1] is type
					switch (i[1]) {
					case "radio":
						if(this.isRadioDisable(i[0])) {
							if(i[2].equals("enable"))
								return false;
							break;
						}else {
							if(i[2].equals("disable"))
								return false;
							break;
						}
					case "checkbox":
						if(this.isCheckBoxDisable(i[0])) {
							if(i[2].equals("enable")) {
								return false;
							}
							break;
						}else {
							if(i[2].equals("disable")) {
								return false;
							}
							break;
						}
					case "other":
						if(this.isTextOnAddWindowDisable(i[0])) {
							if(i[2].equals("enable"))
								return false;
							break;
						}else {
							if(i[2].equals("disable"))
								return false;
							break;
						}
					default:
						Logger.info(String.format("Type of input is incorrect"));
						return false;
					}
				}
			}
			return true;
		} catch (Exception e) {
			Logger.info(String.format("Has error with control: %s", e.getMessage()));
			throw e;
		}
	}
	
	public boolean isTextOnAddWindowDisable(String txt) {
		try {
			this.dynamicTextInput(txt).waitForVisibility();
			if (this.dynamicTextInput(txt).isEnabled()) {
				return false;
			}
			return true;
		} catch (Exception e) {
			Logger.info(String.format("Has error with control: %s", e.getMessage()));
			throw e;
		}
	}	
}
