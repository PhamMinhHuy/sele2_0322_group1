package Administer;

import ElementWrapper.Element;
import ElementWrapper.TableElement;
import TAPopup.CreateProfilesPage;
import common.Constant;
import common.Utilities;

public class DataProfilesPage extends CreateProfilesPage{
	
	private final Element addNewBtn = new Element("//a[normalize-space(text())='Add New']");
	private final TableElement panelViewTable = new TableElement("//table[@class='GridView']");
	private final Element generalSettingsTxt = new Element("//td[normalize-space(text())='General Settings']");
	private final Element profileTxt(String path) {return new Element("//a[normalize-space(text())='"+path+"']");}
	
	public void clickAddNew() {
		this.addNewBtn.clickVisibleElement(Constant.TIMEOUT);
	}
	
	public void waitForGenaeralSettingsDisplay() {
		this.generalSettingsTxt.waitForVisibility(Constant.TIMEOUT);
	}

	public void waitForProfileVisible(String displayName) {
		this.profileTxt(displayName).waitForVisibility();
	}
	
	public void waitForProfileInVisible(String displayName) {
		this.profileTxt(displayName).waitForInvisibility(Constant.TIMEOUT);
	}
	
	public String getValueOfItemOnProfileTable(String header, int column) {
		this.panelViewTable.waitForVisibility();
		return this.panelViewTable.getValueOfCell(header, column);
	}
	
	public void clickEditForObject(String header, String profileName) {
		this.panelViewTable.waitForVisibility();
		int columnOfObject = this.panelViewTable.getColumnByHeader(header, profileName);
		int indexOfObject = this.panelViewTable.getIndexOfHeader("Action");
		this.panelViewTable.clickItem(indexOfObject, columnOfObject, "Edit");
	}
	
	public void clickDeleteForObject(String header, String profileName) {
		this.panelViewTable.waitForVisibility();
		int columnOfObject = this.panelViewTable.getColumnByHeader(header, profileName);
		int indexOfObject = this.panelViewTable.getIndexOfHeader("Action");
		this.panelViewTable.clickItem(indexOfObject, columnOfObject, "Delete");
		Utilities.acceptAlert();
		this.waitForProfileInVisible(profileName);
	}
	
	
}
