package Administer;

import ElementWrapper.Element;
import ElementWrapper.TableElement;
import TAPopup.CreatePanelsPage;
import common.Constant;
import common.Utilities;

public class PanelsPage extends CreatePanelsPage{
	
	private final Element addNewBtn = new Element("//a[normalize-space(text())='Add New']");
	private final TableElement panelViewTable = new TableElement("//table[@class='GridView']");
	private final Element panelsTxt(String path) {return new Element("//a[normalize-space(text())='"+path+"']");}
	
	public void clickAddNew() {
		this.addNewBtn.clickVisibleElement(Constant.TIMEOUT);
	}

	public void waitForPanelsVisible(String displayName) {
		this.panelsTxt(displayName).waitForVisibility();
	}

	public boolean isPanelExist(String displayName) {
		return this.panelsTxt(displayName).isDisplayed();
	}
	
	public void waitForPanelsInVisible(String displayName) {
		this.panelsTxt(displayName).waitForInvisibility(Constant.TIMEOUT);
	}
	
	public String getValueOfItemOnPanelTable(String header, int column) {
		this.panelViewTable.waitForVisibility();
		return this.panelViewTable.getValueOfCell(header, column);
	}
	
	public void clickEditForObject(String header, String panelName) {
		this.panelViewTable.waitForVisibility();
		this.waitForPanelsVisible(panelName);
		int columnOfObject = this.panelViewTable.getColumnByHeader(header, panelName);
		int indexOfObject = this.panelViewTable.getIndexOfHeader("Action");
		this.panelViewTable.clickItem(indexOfObject, columnOfObject, "Edit");
	}
	
	public void clickDeleteForObject(String header, String panelName) {
		this.panelViewTable.waitForVisibility();
		this.waitForPanelsVisible(panelName);
		int columnOfObject = this.panelViewTable.getColumnByHeader(header, panelName);
		int indexOfObject = this.panelViewTable.getIndexOfHeader("Action");
		this.panelViewTable.clickItem(indexOfObject, columnOfObject, "Delete");
		Utilities.acceptAlert();
		this.waitForPanelsInVisible(panelName);
	}
	
}
