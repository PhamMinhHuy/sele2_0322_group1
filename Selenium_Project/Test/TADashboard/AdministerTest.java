package TADashboard;

import static org.testng.Assert.assertTrue;

import org.testng.annotations.Test;

import TAObject.Account;
import common.Constant;
import common.Logger;
import common.Utilities;

public class AdministerTest extends TestBase{
	
	@Test
	public void DA_PANEL_TC028() {
		Logger.info("DA_PANEL_TC028 - Verify that user can login specific repository successfully via Dashboard login page with correct credentials");
		Account Account = new Account(Constant.USERNAMETA,Constant.PASSWORDTA);
		
		Logger.info("Step 1: Navigate to Dashboard login page & Login with valid account");
		loginPage.login(Account);		

		Logger.info("Step 2: Click Administer link & Click Panel link");
		loginPage.goToAdministerPanelPage();
		
		Logger.info("Step 3: Click Add New link");
		panelsPage.clickAddNew();	
		
		Logger.info("Step 4: Try to click other controls when Add New Panel dialog is opening and Observe the current page");
		Logger.info("Verify point 4: All control/form are disabled or locked when Add New Panel dialog is opening");
		assertTrue(panelsPage.isOtherControlerDisable(), "The Controler is not disable");
	}
	
	@Test
	public void DA_PANEL_TC029() {
		Logger.info("DA_PANEL_TC029 - Verify that user is unable to create new panel when (*) required field is not filled");
		Account Account = new Account(Constant.USERNAMETA,Constant.PASSWORDTA);
		
		Logger.info("Step 1: Navigate to Dashboard login page & Select specific repository");
		Logger.info("Step 2: Enter valid username and password & Click on Login button");
		loginPage.login(Account);	

		Logger.info("Step 3: Click on Administer/Panels link & Click on \"Add new\" link");
		loginPage.goToAdministerPanelPage();
		panelsPage.clickAddNew();	
		
		Logger.info("Step 4: Click on OK button");
		panelsPage.clickOkButton();
		
		Logger.info("Step 5: Check warning message show up.");
		Logger.info("Verify point 5: Warning message: \"Display Name is a required field.\" show up");
		assertTrue(Utilities.checkMessageExist(Constant.ERROR_MESSAGE_1), "The error message should be displayed");

	}

	@Test
	public void DA_PANEL_TC030() {
		Logger.info("DA_PANEL_TC030 - Verify that no special character except '@' character is allowed to be inputted into \"Display Name\" field");
		Account Account = new Account(Constant.USERNAMETA,Constant.PASSWORDTA);
		
		Logger.info("Step 1: Navigate to Dashboard login page & Login with valid account");
		loginPage.login(Account);
		
		Logger.info("Step 2: Click Administer link & Click Panel link");
		loginPage.goToAdministerPanelPage();
		
		Logger.info("Step 3: Click Add New link");
		panelsPage.clickAddNew();
		
		Logger.info("Step 4: Enter value into Display Name field with special characters except \"@\" and Click Ok button");
		panelsPage.inputValueOnAddNewPanelWindow(Constant.DISPLAY_NAME_TC030, Constant.NULL, Constant.NULL, null, null);
		
		Logger.info("Step 5: Observe the current page");
		Logger.info("Verify point 5: Message \"Invalid display name. The name cannot contain high ASCII characters or any of the following characters: /:*?<>|\\\"#[]{}=%;\" is displayed");

		Logger.bug("bug: ","this Tcs fail by Bug: Actual Value on Message Box is not correct with Expected");
		assertTrue(Utilities.checkMessageExist(Constant.ERROR_MESSAGE_TC_030), "The error message should be displayed");

//		Click Add New link
//		Enter value into Display Name field with special character is @
//		Observe the current page
//		The new panel is created
	}
	@Test
	public void DA_PANEL_TC031() {
		Logger.info("DA_PANEL_TC031 - Verify that correct panel setting form is displayed with corresponding panel type selected");
		Account Account = new Account(Constant.USERNAMETA,Constant.PASSWORDTA);
		
		Logger.info("Step 1: Navigate to Dashboard login page & Login with valid account");
		loginPage.login(Account);
		
		Logger.info("Step 2: Click Administer link & Click Panel link");
		loginPage.goToAdministerPanelPage();
		
		Logger.info("Step 3: Click Add New link");
		panelsPage.clickAddNew();
		
		Logger.info("Step 4: Verify that chart panel setting form is displayed with corresponding panel type selected");
		panelsPage.selectTypeAddNewPanelWindow("Chart");
		assertTrue(panelsPage.isPanelSettingCorrectWithValue("Chart Settings"));
		
		Logger.info("Step 5: Select Indicator type and Verify that indicator panel setting form is displayed with corresponding panel type selected");
		panelsPage.selectTypeAddNewPanelWindow("Indicator");
		assertTrue(panelsPage.isPanelSettingCorrectWithValue("Indicator Settings"));
		
		Logger.info("Step 6: Select Report type and Verify that report panel setting form is displayed with corresponding panel type selected");
		Logger.info("Report panel setting form is no longer available.");
		
		Logger.info("Step 7: Select Heat Maps type and Verify that heatmap panel setting form is displayed with corresponding panel type selected");
		panelsPage.selectTypeAddNewPanelWindow("Heat Map");
		assertTrue(panelsPage.isPanelSettingCorrectWithValue("Heat Map Settings"));
	}
	
	@Test
	public void DA_PANEL_TC032() {
		Logger.info("DA_PANEL_TC032 - Verify that user is not allowed to create panel with duplicated \"Display Name\" ");
		Account Account = new Account(Constant.USERNAMETA,Constant.PASSWORDTA);
		
		Logger.info("Step 1: Navigate to Dashboard login page & Login with valid account");
		loginPage.login(Account);
		
		Logger.info("Step 2: Click Administer link & Click Panel link");
		loginPage.goToAdministerPanelPage();
		
		Logger.info("Step 3: Click Add New link");
		panelsPage.clickAddNew();
		
		Logger.info("Step 4: Enter display name to \"Display name\" field. and Click on OK button");
		panelsPage.inputValueOnAddNewPanelWindow(Constant.DISPLAY_NAME_TC032, "Name", null, null, null);
		
		Logger.info("Step 5:Click on Add new link again & Enter display name same with previous display name to \"display name\" field.");
		panelsPage.waitForPanelsVisible(Constant.DISPLAY_NAME_TC032);
		panelsPage.clickAddNew();
		panelsPage.inputValueOnAddNewPanelWindow(Constant.DISPLAY_NAME_TC032, "Name", null, null, null);

		Logger.info("Step 6:Click on OK button & Check warning message show up");
		Logger.bug("bug","this Tcs fail by Bug: Actual Value on Message Box is not correct with Expected");
		assertTrue(Utilities.checkMessageExist(Constant.ERROR_MESSAGE_TC_032), "The error message should be displayed");
		
		panelsPage.clickDeleteForObject("Panel Name", Constant.DISPLAY_NAME_TC032);
	}
	@Test
	public void DA_PANEL_TC033() {
		Logger.info("DA_PANEL_TC033 - Verify that \"Data Profile\" listing of \"Add New Panel\" and \"Edit Panel\" control/form are in alphabetical order");
		Account Account = new Account(Constant.USERNAMETA,Constant.PASSWORDTA);
		
		Logger.info("Step 1: Navigate to Dashboard login page & Login with valid account");
		loginPage.login(Account);
		
		Logger.info("Step 2: Click Administer link & Click Panel link");
		loginPage.goToAdministerPanelPage();
		
		Logger.info("Step 3: Click Add New link");
		panelsPage.clickAddNew();
		
		Logger.info("Step 4: Verify that Data Profile list is in alphabetical order");
		assertTrue(panelsPage.isSelectOptionsInAlphabeticalOrder()," The Data Profile list is not in alphabetical order");
		
		Logger.info("Step 5: Enter a display name to display name field & Click on OK button");
		panelsPage.clickAddNew();
		panelsPage.inputValueOnAddNewPanelWindow(Constant.DISPLAY_NAME_TC033, "Name", "Chart", null, null);
		
		Logger.info("Step 6: Click on Edit link");
		panelsPage.clickEditForObject("Panel Name", Constant.DISPLAY_NAME_TC033);

		Logger.info("Step 7: Verify that Data Profile list is in alphabetical order");
		assertTrue(panelsPage.isSelectOptionsInAlphabeticalOrder()," The Data Profile list is not in alphabetical order");
		panelsPage.clickCancelButton();
		
		Logger.info("Step 8: Delete object");
		panelsPage.clickDeleteForObject("Panel Name", Constant.DISPLAY_NAME_TC033);
	}
	
	@Test
	public void DA_PANEL_TC034() {
		Logger.info("DA_PANEL_TC034 - Verify that newly created data profiles are populated correctly under the \"Data Profile\" dropped down menu in  \"Add New Panel\" and \"Edit Panel\" control/form");
		Account Account = new Account(Constant.USERNAMETA,Constant.PASSWORDTA);
		
		Logger.info("Step 1: Navigate to Dashboard login page & Login with valid account");
		loginPage.login(Account);
		
		Logger.info("Step 2: Click on Administer/Data Profiles link");
		loginPage.goToAdministerProfilesPage();
		
		Logger.info("Step 3: Click Add New link");
		profilePage.clickAddNew();
		
		Logger.info("Step 4: Enter name to Name textbox & Click on Finish button");
		profilePage.inputValueOnAddNewProfilesWindow(Constant.PROFILES_NAME_TC034, null, null);
		
		Logger.info("Step 5: Click on Administer/Panels link & Click on add new link");
		profilePage.goToAdministerPanelPage();
		panelsPage.clickAddNew();
		
		Logger.info("Step 6: Verify that \"giang - data\" data profiles are populated correctly under the \"Data Profile\" dropped down menu.");
		Logger.info("Verify point Step 6: giang - data data profiles are populated correctly under the \"Data Profile\" dropped down menu.");

		assertTrue(panelsPage.isDataProfilesPopulatedInSelectOptions(Constant.PROFILES_NAME_TC034),"The Data Profile list is not contant Profiles");

		Logger.info("Step 7: Enter display name to Display Name textbox & Click Ok button to create a panel");
		panelsPage.clickAddNew();
		panelsPage.inputValueOnAddNewPanelWindow(Constant.DISPLAY_NAME_TC033, "Name", null, Constant.PROFILES_NAME_TC034, null);
		
		Logger.info("Step 8: Click on edit link & Verify that \"giang - data\" data profiles are populated correctly under the \"Data Profile\" dropped down menu.");
		Logger.info("Verify point Step 8: giang - data data profiles are populated correctly under the \"Data Profile\" dropped down menu.");
		panelsPage.clickEditForObject("Panel Name", Constant.DISPLAY_NAME_TC033);
		assertTrue(panelsPage.isDataProfilesPopulatedInSelectOptions(Constant.PROFILES_NAME_TC034),"The Data Profile list is not contant Profiles");

		Logger.info("Step 9: Delete object");
		panelsPage.clickDeleteForObject("Panel Name", Constant.DISPLAY_NAME_TC033);
		loginPage.goToAdministerProfilesPage();
		profilePage.clickDeleteForObject("Data Profile", Constant.PROFILES_NAME_TC034);
		
	}
	@Test
	public void DA_PANEL_TC035() {
		Logger.info("DA_PANEL_TC035 - Verify that no special character except '@' character is allowed to be inputted into \"Chart Title\" field");
		Account Account = new Account(Constant.USERNAMETA,Constant.PASSWORDTA);
		
		Logger.info("Step 1: Navigate to Dashboard login page & Login with valid account");
		loginPage.login(Account);
		
		Logger.info("Step 2: Click on Administer/Panel link");
		panelsPage.goToAdministerPanelPage();
		
		Logger.info("Step 3: Click Add New link");
		panelsPage.clickAddNew();
		
		Logger.info("Step 4: Enter value into Display Name field & Enter value into Chart Title field with special characters except \"@\"");
		panelsPage.inputValueOnAddNewPanelWindow(Constant.DISPLAY_NAME, "Name", null, null, Constant.CHART_TITLE_TC035);
		
		Logger.info("this Tcs fail by Bug: Actual Value on Message Box is not correct with Expected");
		Logger.info("Step 5: Click Ok button & Observe the current page");
		Logger.info("Verify point Step 5: Message \"Invalid title name. The name cannot contain high ASCII characters or any of the following characters: /:*?<>|\\\"#[]{}=%;\" is displayed");
		assertTrue(Utilities.checkMessageExist(Constant.ERROR_MESSAGE_TC_035), "The error message should be displayed");
		
		Logger.info("Step 6: Click Add New link, Enter value into Display Name field & Chart Title field with special character is @");
		panelsPage.inputValueOnAddNewPanelWindow(Constant.DISPLAY_NAME2_TC030, "Name", null, null, Constant.CHART_TITLE_TC035);

		Logger.info("Step 7: Observe the current page");
		Logger.info("Verify point Step 7: The new panel is created");
		assertTrue(panelsPage.isPanelExist(Constant.PROFILES_NAME_TC034),"The panel is not exist");
		panelsPage.clickDeleteForObject("Panel Name", Constant.PROFILES_NAME_TC034);

	}
	@Test
	public void DA_PANEL_TC036() {
		Logger.info("DA_PANEL_TC036 - Verify that all chart types ( Pie, Single Bar, Stacked Bar, Group Bar, Line ) are listed correctly under \"Chart Type\" dropped down menu.");
		Account Account = new Account(Constant.USERNAMETA,Constant.PASSWORDTA);
		String sPageName = "main_hung";
		
		Logger.info("Step 1: Navigate to Dashboard login page & Login with valid account");
		loginPage.login(Account);
		
		Logger.info("Step 2: Click 'Add Page' link");
		panelsPage.openAddpagePopup();

		Logger.info("Step 3: Enter Page Name field");
		addPagePopup.enterPageName(sPageName);
		
		Logger.info("Step 4: Click OK button");
		loginPage.clickOkButton();
		
		Logger.info("Step 5: Click 'Create new panel' button");
		loginPage.selectPage(sPageName);
		loginPage.createPanel();
		
		Logger.info("Step 6: Click 'Chart Type' drop-down menu & Check that 'Chart Type' are listed 5 options: 'Pie', 'Single Bar', 'Stacked Bar', 'Group Bar' and 'Line'");
		Logger.info("Verify point Step 6: 'Chart Type' are listed 5 options: 'Pie', 'Single Bar', 'Stacked Bar', 'Group Bar' and 'Line'");
		assertTrue(createPanelsPage.isChartTypePopulatedInSelectOptions("Pie"),"The drop-down is not contant expected value");
		assertTrue(createPanelsPage.isChartTypePopulatedInSelectOptions("Single Bar"),"The drop-down is not contant expected value");
		assertTrue(createPanelsPage.isChartTypePopulatedInSelectOptions("Stacked Bar"),"The drop-down is not contant expected value");
		assertTrue(createPanelsPage.isChartTypePopulatedInSelectOptions("Group Bar"),"The drop-down is not contant expected value");
		assertTrue(createPanelsPage.isChartTypePopulatedInSelectOptions("Line"),"The drop-down is not contant expected value");
		createPanelsPage.clickCancelButton();
		loginPage.deletePage(sPageName);
	}
	@Test
	public void DA_PANEL_TC037() {
		Logger.info("DA_PANEL_TC037 - Verify that \"Category\", \"Series\" and \"Caption\" field are enabled and disabled correctly corresponding to each type of the \"Chart Type\"");
		Account Account = new Account(Constant.USERNAMETA,Constant.PASSWORDTA);
		String sPageName = "main_hung";
		
		Logger.info("Step 1: Navigate to Dashboard login page & Login with valid account");
		loginPage.login(Account);
		
		Logger.info("Step 2: Click 'Add Page' link");
		panelsPage.openAddpagePopup();

		Logger.info("Step 3: Enter Page Name field");
		addPagePopup.enterPageName(sPageName);
		
		Logger.info("Step 4: Click OK button");
		loginPage.clickOkButton();
		
		Logger.info("Step 5: Click 'Create new panel' button");
		loginPage.selectPage(sPageName);
		loginPage.createPanel();
		
		Logger.info("Step 6: Select 'Pie' Chart Type and Check that 'Category' and 'Caption' are disabled, 'Series' is enabled");
		createPanelsPage.selectChartTypeAddNewPanelWindow("Pie");
		assertTrue(createPanelsPage.checkInputStatus("Category,other,disable;Series,other,enable"),"The input is not correct with status");

		Logger.info("Step 7: Select 'Single Bar' Chart Type and Check that 'Category' and Check that 'Category' is disabled, 'Series' and 'Caption' are enabled");
		createPanelsPage.selectChartTypeAddNewPanelWindow("Single Bar");
		assertTrue(createPanelsPage.checkInputStatus("Category,other,disable;Series,other,enable"),"The input is not correct with status");

		Logger.info("Step 8: Select 'Stacked Bar' Chart Type and Check that Check that 'Category' ,'Series' and 'Caption' are enabled");
		createPanelsPage.selectChartTypeAddNewPanelWindow("Stacked Bar");
		assertTrue(createPanelsPage.checkInputStatus("Category,other,enable;Series,other,enable"),"The input is not correct with status");

		Logger.info("Step 8: Select 'Group Bar' Chart Type and Check that Check that 'Category' ,'Series' and 'Caption' are enabled");
		createPanelsPage.selectChartTypeAddNewPanelWindow("Group Bar");
		assertTrue(createPanelsPage.checkInputStatus("Category,other,enable;Series,other,enable"),"The input is not correct with status");

		Logger.info("Step 8: Select 'Line' Chart Type and Check that Check that 'Category' ,'Series' and 'Caption' are enabled");
		createPanelsPage.selectChartTypeAddNewPanelWindow("Line");
		assertTrue(createPanelsPage.checkInputStatus("Category,other,enable;Series,other,enable"),"The input is not correct with status");
		createPanelsPage.clickCancelButton();
		loginPage.deletePage(sPageName);

	}
}