package TADashboard;

import java.lang.reflect.Method;

import org.testng.ITestContext;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Parameters;

import Administer.DataProfilesPage;
import Administer.PanelsPage;
import TADashboard.LoginPage;
import TADashboard.MainPage;
import TAPopup.AddPagePopup;
import TAPopup.CreatePanelsPage;
import common.Constant;
import common.DriverType;
import driverWrapper.DriverManager;

public class TestBase {
	
	TADashboard.LoginPage loginPage = new LoginPage();
	TADashboard.MainPage mainPage = new MainPage();
	Administer.PanelsPage panelsPage = new PanelsPage();
	Administer.DataProfilesPage profilePage = new DataProfilesPage();
	TAPopup.AddPagePopup addPagePopup = new AddPagePopup();
	TAPopup.CreatePanelsPage createPanelsPage = new CreatePanelsPage();
	
	@Parameters({"browser", "remote"})
	@BeforeMethod(alwaysRun = true)
	public void beforeMethod( Boolean remote, String hub, ITestContext context, Method method) {
		common.ExtentTestManager.startTest(method.getName(), common.TestListener.testSuite.get(context.getName()));
		 System.out.println("Pre-condition: Instantiate Webdriver");
		DriverManager.createDriver(DriverType.Chrome, remote, hub);
		DriverManager.getDriver().manage().window().maximize();
		DriverManager.getDriver().navigate().to(Constant.RAILWAY_URL);
		
	}
	
	@AfterMethod
	public void afterMethod() {
		mainPage.logout();
		DriverManager.getDriver().quit();
	}
}
