package TADashboard;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertTrue;

import org.testng.annotations.Test;

import TAObject.Account;
import common.Constant;
import common.Logger;
import common.Utilities;

public class LoginTest extends TestBase{
	
	@Test
	public void DA_LOGIN_TC001() {
		Logger.info("DA_LOGIN_TC001 - Verify that user can login specific repository successfully via Dashboard login page with correct credentials");
		Account Account = new Account(Constant.USERNAMETA,Constant.PASSWORDTA);
		
		Logger.info("Step 1: Navigate to Dashboard login page & Login with valid account");
		loginPage.login(Account);		
		
		Logger.info("Step 2: Verify that Dashboard Mainpage appears");
		boolean isLoginSucess = mainPage.isLoginSucess(Account.getid());
		assertTrue(isLoginSucess,"The Dashboard Mainpage is not appears");
	}
	
	@Test
	public void DA_LOGIN_TC002() {
		Logger.info("DA_LOGIN_TC002 - Verify that user fails to login specific repository successfully via Dashboard login page with incorrect credentials");
		Account Account = new Account(Constant.INVALID_USERNAMETA,Constant.INVALID_PASSWORDTA);
		
		Logger.info("Step 1: Navigate to Dashboard login page & Login with valid account");
		loginPage.login(Account);		
		
		Logger.info("Step 2: Verify that Dashboard Error message \"Username or password is invalid\" appears");
		assertTrue(Utilities.checkMessageExist(Constant.ERROR_MESSAGE)," The Dashboard Error message \\\"Username or password is invalid\\\" appears is not appears");
	}

	@Test
	public void DA_LOGIN_TC003() {
		Logger.info("DA_LOGIN_TC003 - Verify that user fails to log in specific repository successfully via Dashboard login page with correct username and incorrect password");
		Account Account = new Account(Constant.USERNAMETA,Constant.INVALID_PASSWORDTA);
		
		Logger.info("Step 1: Navigate to Dashboard login page & Login with valid account");
		loginPage.login(Account);		
		
		Logger.info("Step 2: Verify that Dashboard Error message \"Username or password is invalid\" appears");
		assertTrue(Utilities.checkMessageExist(Constant.ERROR_MESSAGE)," The Dashboard Error message \\\"Username or password is invalid\\\" appears is not appears");
	}
	
	@Test
	public void DA_LOGIN_TC004() {
		Logger.info("DA_LOGIN_TC004 - Verify that user is able to log in different repositories successfully after logging out current repository");
		Account Account = new Account(Constant.USERNAMETA,Constant.PASSWORDTA);
		
		Logger.info("Step 1: Navigate to Dashboard login page & Login with valid account");
		loginPage.login(Account);

		Logger.info("Step 2: Click on \"Logout\" button & Select a different repository");
		mainPage.logout();
		loginPage.selectRepository(Constant.REPOSITORY_NAME_1);

		Logger.info("Step 3: Enter valid username and password of this repository");
		loginPage.login(Account);
		
		Logger.info("Step 4: Verify that Dashboard Mainpage appears");
		boolean isLoginSucess = mainPage.isLoginSucess(Account.getid());
		assertTrue(isLoginSucess,"The Dashboard Mainpage is not appears");
	}
	
	@Test
	public void DA_LOGIN_TC005() {
		Logger.info("DA_LOGIN_TC005 - Verify that there is no Login dialog when switching between 2 repositories with the same account");
		Account Account = new Account(Constant.USERNAMETA,Constant.PASSWORDTA);

		Logger.info("Step 1: Navigate to Dashboard login page & Login with valid account");
		loginPage.login(Account);
		
		Logger.info("Step 2: Choose another repository in Repository list");
		mainPage.selectRepositoryOnMainPage(Constant.REPOSITORY_NAME_1);
		
		Logger.info("Step 3: Observe the current page: There is no Login Repository dialog");
		boolean isLoginSucess = mainPage.isLoginSucess(Account.getid());
		assertTrue(isLoginSucess);
		
		Logger.info("Step 3: Observe the current page: The Repository menu displays name of switched repository");
		assertEquals(mainPage.getCurrentRepositoryNameOnMainPage(),Constant.REPOSITORY_NAME_1);
		
	}
	
	@Test
	public void DA_LOGIN_TC006() {
		Logger.info("DA_LOGIN_TC006 - Verify that \"Password\" input is case sensitive");
		Account Account1 = new Account(Constant.USERNAMETA1,Constant.PASSWORDTA1_UPPERCASE);
		Account Account2 = new Account(Constant.USERNAMETA1,Constant.PASSWORDTA1_LOWSERCASE);
		
		Logger.info("Step 1: Navigate to Dashboard login page & Login with valid account");
		loginPage.login(Account1);
		
		Logger.info("Step 2: Observe the current page is MainPage");
		boolean isLoginSucess = mainPage.isLoginSucess(Account1.getid());
		assertTrue(isLoginSucess);
		
		Logger.info("Step 3: Logout TA Dashboard");
		mainPage.logout();
		
		Logger.info("Step 4: Login with the above account but enter lowercase password & Observe the current page");
		loginPage.login(Account2);
		
		Logger.info("Verify point Step 4: Dashboard Error message \"Username or password is invalid\" appears");
		assertTrue(Utilities.checkMessageExist(Constant.ERROR_MESSAGE));
	}
	
	@Test
	public void DA_LOGIN_TC007() {
		Logger.info("DA_LOGIN_TC007 - Verify that \"Username\" is not case sensitive");
		Account Account1 = new Account(Constant.USERNAMETA1,Constant.PASSWORDTA1_UPPERCASE);
		Account Account2 = new Account(Constant.USERNAMETA1_UPPERCASE,Constant.PASSWORDTA1_UPPERCASE);
		
		Logger.info("Step 1: Navigate to Dashboard login page & Login with valid account");
		loginPage.login(Account1);
		
		Logger.info("Step 2: Observe the current page is MainPage");
		boolean isLoginSucess = mainPage.isLoginSucess(Account1.getid());
		assertTrue(isLoginSucess);
		
		Logger.info("Step 3: Logout TA Dashboard");
		mainPage.logout();
		
		Logger.info("Step 4: Login with the above account but enter lowercase password & Observe the current page is Main Page");
		loginPage.login(Account1);
		assertTrue(mainPage.isLoginSucess(Account1.getid()));
	}
	@Test
	public void DA_LOGIN_TC008_009() {
		Logger.info("DA_LOGIN_TC008 - Verify that password with special characters is working correctly");
		Account Account1 = new Account(Constant.USERNAMETA_TC008,Constant.PASSWORDTA_TC008);
		
		Logger.info("Step 1: Navigate to Dashboard login page & Login with valid account");
		loginPage.login(Account1);
		
		Logger.info("Step 2: Observe the current page is MainPage");
		boolean isLoginSucess1 = mainPage.isLoginSucess(Account1.getid());
		assertTrue(isLoginSucess1);		
		mainPage.logout();
		
		Logger.info("DA_LOGIN_TC009 - Verify that username with special characters is working correctly");

		Account Account2 = new Account(Constant.USERNAMETA_TC009,Constant.PASSWORDTA_TC009);
		
		Logger.info("Step 1: Navigate to Dashboard login page & Login with valid account");
		loginPage.login(Account2);
		
		Logger.info("Step 2: Observe the current page is MainPage");
		boolean isLoginSucess2 = mainPage.isLoginSucess(Account2.getid());
		assertTrue(isLoginSucess2);
	}
	@Test
	public void DA_LOGIN_TC010() {
		Logger.info("DA_LOGIN_TC010 - Verify that the page works correctly for the case when no input entered to Password and Username field");

		Logger.info("Step 1: Navigate to Dashboard login page & Click Login button without entering data into Username and Password field");
		loginPage.loginWithOutAccount();
		
		Logger.info("Step 2: Observe the current page: There is a message \"Please enter username\" is popup");
		Logger.bug("bug 01","This TestCase fail by Bug");
		assertTrue(Utilities.checkMessageExist(Constant.ERROR_MESSAGE_TC010),"The error message should be displayed");

	}
}