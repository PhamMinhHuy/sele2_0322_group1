package main;

import static org.testng.Assert.*;

import org.testng.Assert;
import org.testng.annotations.Test;

import main.MainTest;
import TADashboard.LoginPage;
import TADashboard.MainPage;
import TADashboard.TestBase;
import TAObject.Account;
import TAPopup.AddPagePopup;
import common.Constant;
import common.Utilities;
import common.Logger;
import driverWrapper.DriverManager;

public class MainTest extends TestBase{ 
	private LoginPage loginPage = new LoginPage();
	private MainPage mainPage;
	private AddPagePopup addPagePopup;
	/*
	 * Verify that the page works correctly for the case when no input entered to Password and Username field
	 * Dinh Huynh
	 */
	@Test
	public void DA_MP_TC011() {
		Account account = new Account(Constant.USERNAMETA, Constant.PASSWORDTA);
		common.Logger.info("Step 1: Navigate to Dashboard login page");
		Logger.info("Step 2: Login with valid account");
		mainPage = loginPage.login(account);
		
		Logger.info("Step 3: Go to Global Setting -> Add page");
		addPagePopup = mainPage.openAddpagePopup();
		
		Logger.info("Step 4: Try to go to Global Setting -> Add page again");
		try {
			addPagePopup.waitForDisplayedAddPagePopup();
			addPagePopup = mainPage.openAddpagePopup();
		} catch (Exception e) {
			Logger.warning(e.getMessage());
		}
		
		Logger.info("Step 5: Observe the current page");
		assertFalse(mainPage.isEnableGlobalSettingButton(), "Blobal Setting button should be disable");
		
		Logger.info("Post-Condition: Logout");
	}
	
	/*
	 * Verify that user is able to add additional pages besides "Overview" page successfully
	 * Dinh Huynh
	 */
	@Test
	public void DA_MP_TC012() {
		Account account = new Account(Constant.USERNAMETA, Constant.PASSWORDTA);
		String sPageName = "Test";
		String sPageNameOld = "Overview";
		Logger.info("Step 1: Navigate to Dashboard login page");
		Logger.info("Step 2: Login with valid account");
		mainPage = loginPage.login(account);
		
		Logger.info("Step 3: Go to Global Setting -> Add page");
		mainPage.waitForMainPageLoading();
		addPagePopup = mainPage.openAddpagePopup();
		
		Logger.info("Step 4: Enter Page Name field");
		addPagePopup.enterPageName(sPageName);
		
		Logger.info("Step 5: Click OK button");
		mainPage = addPagePopup.clickOkButton();
		mainPage.waitForLoading(10);
		
		Logger.info("Step 6: Check \"Test\" page is displayed besides \"Overview\" page");
		assertTrue(mainPage.isDisplayedPageNew(sPageName), "New 'Test' Page is displayed");
		assertTrue(mainPage.isDisplayedPageNewBesidesOldPage(sPageName, sPageNameOld), "New 'Test' Page is displayed besides 'Overview' page");
		
		Logger.info("Post-Condition: Delete newly added page");
		mainPage.waitForLoading(10);
		mainPage.clickOnPageLink(sPageName);
		mainPage.clickDeleteLink();
		Utilities.acceptAlert();
	}
	
	/*
	 * Verify that the newly added main parent page is positioned at the location specified 
	 * as set with "Displayed After" field of "New Page" form on the main page bar/"Parent Page" dropped down menu
	 * Dinh Huynh
	 */
	@Test
	public void DA_MP_TC013() {
		Account account = new Account(Constant.USERNAMETA, Constant.PASSWORDTA);
		String sPage1 = "Page 1";
		String sPage2 = "Page 2";
		Logger.info("Step 1: Navigate to Dashboard login page");
		Logger.info("Step 2: Login with valid account");
		mainPage = loginPage.login(account);
		
		Logger.info("Step 3: Go to Global Setting -> Add page");
		mainPage.waitForMainPageLoading();
		addPagePopup = mainPage.openAddpagePopup();
		
		Logger.info("Step 4: Enter Page Name field: Page 1");
		addPagePopup.enterPageName(sPage1);
		
		Logger.info("Step 5: Click OK button");
		mainPage = addPagePopup.clickOkButton();
		
		Logger.info("Step 6: Go to Global Setting -> Add page");
		addPagePopup = mainPage.openAddpagePopup();
		
		Logger.info("Step 7: Enter Page Name field: Page 2");
		addPagePopup.enterPageName(sPage2);
		
		Logger.info("Step 8: Click on Displayed After dropdown list");
		Logger.info("Step 9: Select specific page");
		addPagePopup.selectDisplayedAfter(sPage1);
		
		Logger.info("Step 10: Click OK button");
		mainPage = addPagePopup.clickOkButton();
		
		Logger.info("Step 11: Check \"Another Test\" page is positioned besides the \"Test\" page");
		assertTrue(mainPage.isDisplayedPageNew(sPage2), "New 'Page 2' Page is displayed");
		assertTrue(mainPage.isDisplayedPageNewBesidesOldPage(sPage2, sPage1), "New 'Test' Page is displayed besides 'Page 1' page");
		
		Logger.info("Post-Condition: Delete newly added main child page and its parent page");
		mainPage.clickOnPageLink(sPage2);
		mainPage.clickDeleteLink();
		Utilities.acceptAlert();
		mainPage.clickOnPageLink(sPage1);
		mainPage.clickDeleteLink();
		Utilities.acceptAlert();
	}
	
	/*
	 * Verify that "Public" pages can be visible and accessed by all users of working repository
	 * Dinh Huynh
	 */
	@Test
	public void DA_MP_TC014() {
		Account account = new Account(Constant.USERNAMETA, Constant.PASSWORDTA);
		Account account2 = new Account(Constant.USERNAMETA_TC008, Constant.PASSWORDTA_TC008);
		String sPageName = "Test";
		Logger.info("Step 1: Navigate to Dashboard login page");
		Logger.info("Step 2: Login with valid account");
		mainPage = loginPage.login(account);
		
		Logger.info("Step 3: Go to Global Setting -> Add page");
		mainPage.waitForMainPageLoading();
		addPagePopup = mainPage.openAddpagePopup();
		
		Logger.info("Step 4: Enter Page Name field");
		addPagePopup.enterPageName(sPageName);
		
		Logger.info("Step 5: Check Public checkbox");
		addPagePopup.checkPublic(true);
		
		Logger.info("Step 6: Click OK button");
		mainPage = addPagePopup.clickOkButton();
		
		Logger.info("Step 7: Click on Log out link");
		mainPage.logout();
		
		Logger.info("Step 8: Log in with another valid account");
		mainPage = loginPage.login(account2);
		
		Logger.info("Step 9: Check newly added page is visibled");
		assertTrue(mainPage.isDisplayedPageNew(sPageName), "New Page is displayed");
		
		Logger.info("Post-Condition: Delete newly added main child page and its parent page");
		mainPage.clickOnPageLink(sPageName);
		mainPage.clickDeleteLink();
		Utilities.acceptAlert();
	}
	
	/*
	 * Verify that non "Public" pages can only be accessed and visible to their creators 
	 * with condition that all parent pages above it are "Public"
	 * Dinh Huynh
	 */
	@Test
	public void DA_MP_TC015() {
		Account account = new Account(Constant.USERNAMETA, Constant.PASSWORDTA);
		Account account2 = new Account(Constant.USERNAMETA_TC008, Constant.PASSWORDTA_TC008);
		String sPageName = "Test";
		String sPageName_Step8 = "Test Child";
		String sPageName_Step8_Verify = "Test${nbsp}Child";
		
		Logger.info("Step 1: Navigate to Dashboard login page");
		Logger.info("Step 2: Login with valid account");
		mainPage = loginPage.login(account);
		
		Logger.info("Step 3: Go to Global Setting -> Add page");
		mainPage.waitForMainPageLoading();
		addPagePopup = mainPage.openAddpagePopup();
		
		Logger.info("Step 4: Enter Page Name field: " + sPageName);
		addPagePopup.enterPageName(sPageName);
		
		Logger.info("Step 5: Check Public checkbox");
		addPagePopup.checkPublic(true);
		
		Logger.info("Step 6: Click OK button");
		mainPage = addPagePopup.clickOkButton();
		
		Logger.info("Step 7: Go to Global Setting -> Add page");
		mainPage.waitForMainPageLoading();
		addPagePopup = mainPage.openAddpagePopup();
		
		Logger.info("Step 8: Enter Page Name field: " + sPageName_Step8);
		addPagePopup.enterPageName(sPageName_Step8);
		
		Logger.info("Step 9: Click on  Select Parent dropdown list");
		Logger.info("Step 10: Select specific page");
		addPagePopup.selectParentPage(sPageName);
		
		Logger.info("Step 11: Click OK button");
		mainPage = addPagePopup.clickOkButton();
		
		Logger.info("Step 12: Click on Log out link");
		mainPage.logout();
		
		Logger.info("Step 13: Log in with another valid account");
		mainPage = loginPage.login(account2);
		
//		Have a bug here
		Logger.info("Step 14: Children is invisibled");
		assertFalse(mainPage.isDisplayedChildPage(sPageName, sPageName_Step8_Verify), "Children Page is is invisibled");
		
		Logger.info("Post-Condition: Log in  as creator page account and delete newly added page and its parent page");
		mainPage.logout();
		loginPage.login(account);
		mainPage.clickOnPageLink(sPageName_Step8);
		mainPage.clickDeleteLink();
		Utilities.acceptAlert();
		mainPage.clickOnPageLink(sPageName);
		mainPage.clickDeleteLink();
		Utilities.acceptAlert();
	}
	
	/*
	 * Verify that user is able to edit the "Public" setting of any page successfully
	 * Dinh Huynh
	 */
	@Test
	public void DA_MP_TC016() {
		Account account = new Account(Constant.USERNAMETA, Constant.PASSWORDTA);
		Account account2 = new Account(Constant.USERNAMETA_TC008, Constant.PASSWORDTA_TC008);
		String sPageName = "Test";
		String sPageName_Step7 = "Another Test";
		String sPageName_Step7_Verify = "Another\\u00A0Test";
		
		Logger.info("Step 1: Navigate to Dashboard login page");
		Logger.info("Step 2: Login with valid account");
		mainPage = loginPage.login(account);
		
		Logger.info("Step 3: Go to Global Setting -> Add page");
		mainPage.waitForMainPageLoading();
		addPagePopup = mainPage.openAddpagePopup();
		
		Logger.info("Step 4: Enter Page Name field");
		addPagePopup.enterPageName(sPageName);
		
		Logger.info("Step 5: Click OK button");
		mainPage = addPagePopup.clickOkButton();
		
		Logger.info("Step 6: Go to Global Setting -> Add page");
		mainPage.waitForMainPageLoading();
		addPagePopup = mainPage.openAddpagePopup();
		
		Logger.info("Step 7: Enter Page Name field");
		addPagePopup.enterPageName(sPageName_Step7);
		
		Logger.info("Step 8: Check Public checkbox");
		addPagePopup.checkPublic(true);
		
		Logger.info("Step 9: Click OK button");
		mainPage = addPagePopup.clickOkButton();
		
		Logger.info("Step 10: Click on \"Test\" page");
		mainPage.clickOnPageLink(sPageName);
		
		Logger.info("Step 11: Click on \"Edit\" link");
		addPagePopup = mainPage.clickEditLink();
		
		Logger.info("Verify point 12: Check \"Edit Page\" pop up window is displayed");
		assertEquals(addPagePopup.getTextPopupTitle(), "Edit Page",
				"'Edit Page' pop up window is displayed");
		
		Logger.info("Step 13: Check Public checkbox");
		addPagePopup.checkPublic(true);
		
		Logger.info("Step 14: Click OK button");
		mainPage = addPagePopup.clickOkButton();
		
		Logger.info("Step 15: Click on \"Another Test\" page");
		mainPage.hoverOnPage(sPageName);
		mainPage.clickOnPageLink(sPageName_Step7_Verify);
		
		Logger.info("Step 16: Click on \"Edit\" link");
		addPagePopup = mainPage.clickEditLink();
		
		Logger.info("Verify point 17: Check \"Edit Page\" pop up window is displayed");
		assertEquals(addPagePopup.getTextPopupTitle(), "Edit Page",
				"'Edit Page' pop up window is displayed");
		
		Logger.info("Step 18: Uncheck Public checkbox");
		addPagePopup.checkPublic(false);
		
		Logger.info("Step 19: Click OK button");
		mainPage = addPagePopup.clickOkButton();
		
		Logger.info("Step 20: Click Log out link");
		mainPage.logout();
		
		Logger.info("Step 21: Log in with another valid account");
		mainPage = loginPage.login(account2);
		
		Logger.info("Verify point 22: Check \"Test\" Page is visible and can be accessed");
		assertTrue(mainPage.isDisplayedPageNew(sPageName), sPageName + " Page is displayed");
		
//		Bug here:  Unpublic page is displayed on an other user main page
		Logger.info("Verify point 23: Check \"Another Test\" page is invisible");
		assertFalse(mainPage.isDisplayedPageNew(sPageName_Step7_Verify), sPageName_Step7_Verify + " Page is invisible");
	}
	
	/*
	 * Verify that user can remove any main parent page except "Overview" page successfully
	 *  and the order of pages stays persistent as long as there is not children page under it
	 * Dinh Huynh
	 */
	@Test
	public void DA_MP_TC017() {
		Account account = new Account(Constant.USERNAMETA, Constant.PASSWORDTA);
		String sOverviewPage = "Overview";
		String sPageName = "Test";
		String sPageName_Step7 = "Test Child";
		String sPageName_Step7_Verify = "Test\\u00A0Child";
		String sAlertMessage = "Are you sure you want to remove this page?";
		String sAlertWarnning = "Can not delete page 'Test' since it has children page(s)";
		
		
		Logger.info("Step 1: Navigate to Dashboard login page");
		Logger.info("Step 2: Log in specific repository with valid account");
		mainPage = loginPage.login(account);
		
		Logger.info("Step 3: Add a new parent page");
		mainPage.waitForMainPageLoading();
		addPagePopup = mainPage.openAddpagePopup();
		mainPage = addPagePopup.addNewParentPage(sPageName, false);
		
		Logger.info("Step 4: Add a children page of newly added page");
		mainPage.waitForMainPageLoading();
		addPagePopup = mainPage.openAddpagePopup();
		mainPage = addPagePopup.addNewChildPage(sPageName, sPageName_Step7, false);
		
		Logger.info("Step 5: Click on parent page");
		mainPage.clickOnPageLink(sPageName);
		
		Logger.info("Step 6: Click \"Delete\" link");
		mainPage.clickDeleteLink();
		
		Logger.info("Verify point 7: Check confirm message \"Are you sure you want to remove this page?\" appears");
		assertEquals(DriverManager.getTexttAlert(), sAlertMessage,
				"Check confirm message 'Are you sure you want to remove this page?' appears");
		
		Logger.info("Step 8: Click OK button");
		DriverManager.acceptAlert();
		
		Logger.info("Verify point 9: Check warning message \"Can not delete page 'Test' since it has children page(s)\" appears");
		assertEquals(DriverManager.getTexttAlert(), sAlertWarnning,
				"Check warning message displayed");
		
		Logger.info("Step 10: Click OK button");
		DriverManager.acceptAlert();
		
		Logger.info("Step 11: Click on  children page");
		mainPage.hoverOnPage(sPageName);
		mainPage.clickOnPageLink(sPageName_Step7_Verify);
		
		Logger.info("Verify point 12: Click \"Delete\" link");
		mainPage.clickDeleteLink();
		
		Logger.info("Verify point 13: Check confirm message \"Are you sure you want to remove this page?\" appears");
		assertEquals(DriverManager.getTexttAlert(), sAlertMessage,
				"Check confirm message 'Are you sure you want to remove this page?' appears");
		
		Logger.info("Step 14: Click OK button");
		DriverManager.acceptAlert();
		
		Logger.info("Verify point 15: Check children page is deleted");
		assertFalse(mainPage.isDisplayedChildPage(sPageName, sPageName_Step7_Verify));
		
		Logger.info("Step 16: Click on  parent page");
		mainPage.clickOnPageLink(sPageName);
		
		Logger.info("Step 17: Click \"Delete\" link");
		mainPage.clickDeleteLink();
		
		Logger.info("Verify point 18: Check confirm message \"Are you sure you want to remove this page?\" appears");
		assertEquals(DriverManager.getTexttAlert(), sAlertMessage,
				"Check confirm message 'Are you sure you want to remove this page?' appears");
		
		Logger.info("Step 19: Click OK button");
		DriverManager.acceptAlert();
		
		Logger.info("Step 20: Check parent page is deleted");
		assertFalse(mainPage.isDisplayedPageNew(sPageName));
		
		Logger.info("Step 21: Click on \"Overview\" page");
		mainPage.clickOnPageLink(sOverviewPage);
		Logger.info("Verify point 22: Check \"Delete\" link disappears");
		assertFalse(mainPage.isDisplayedDeleteLink(), "Check 'Delete' link disappears"); 
	}
	
	/*
	 * Verify that user is able to add additional sibbling pages to the parent page successfully
	 * Dinh Huynh
	 */
	@Test
	public void DA_MP_TC018() {
		Account account = new Account(Constant.USERNAMETA, Constant.PASSWORDTA);
		String sPageName = "Test";
		String sPageName_Step4 = "Test";
		String sPageName_Step7 = "Test Child 1";
		String sPageName_Step12 = "Test Child 2";
		
		Logger.info("Step 1: Navigate to Dashboard login page");
		Logger.info("Step 2: Login with valid account");
		mainPage = loginPage.login(account);
		
		Logger.info("Step 3: Go to Global Setting -> Add page");
		mainPage.waitForMainPageLoading();
		addPagePopup = mainPage.openAddpagePopup();
		
		Logger.info("Step 4: Enter Page Name field: " + sPageName);
		addPagePopup.enterPageName(sPageName);
		
		Logger.info("Step 5: Click OK button");
		mainPage = addPagePopup.clickOkButton();
		
		Logger.info("Step 6: Go to Global Setting -> Add page");
		mainPage.waitForMainPageLoading();
		addPagePopup = mainPage.openAddpagePopup();
		
		Logger.info("Step 7: Enter Page Name field: " + sPageName_Step7);
		addPagePopup.enterPageName(sPageName_Step7);
		
		Logger.info("Step 8: Click on  Parent Page dropdown list");
		Logger.info("Step 9: Select a parent page");
		addPagePopup.selectParentPage(sPageName);
		
		Logger.info("Step 10: Click OK button");
		mainPage = addPagePopup.clickOkButton();
		
		Logger.info("Step 11: Go to Global Setting -> Add page");
		mainPage.waitForMainPageLoading();
		addPagePopup = mainPage.openAddpagePopup();
		
		Logger.info("Step 12: Enter Page Name field: " + sPageName_Step12);
		addPagePopup.enterPageName(sPageName_Step12);
		
		Logger.info("Step 13: Click on  Parent Page dropdown list");
		Logger.info("Step 14: Select a parent page");
		addPagePopup.selectParentPage(sPageName);
		
		Logger.info("Step 15: Click OK button");
		mainPage = addPagePopup.clickOkButton();

		Logger.info("Verify point 16: Check \"Test Child 2\" is added successfully");
		assertFalse(mainPage.isDisplayedChildPage(sPageName, sPageName_Step12), "Children Page is is invisibled");
	}
	
	/*
	 * Verify that user is able to add additional sibbling page levels to the parent page successfully.
	 * Dinh Huynh
	 */
	@Test
	public void DA_MP_TC019() {
		Account account = new Account(Constant.USERNAMETA, Constant.PASSWORDTA);
		String sPageName = "Page 1";
		String sPageNameParent = "Overview";
		
		Logger.info("Step 1: Navigate to Dashboard login page");
		Logger.info("Step 2: Login with valid account");
		mainPage = loginPage.login(account);
		
		Logger.info("Step 3: Go to Global Setting -> Add page");
		mainPage.waitForMainPageLoading();
		addPagePopup = mainPage.openAddpagePopup();
		
		Logger.info("Step 4: Enter info into all required fields on New Page dialog");
		addPagePopup.enterPageName(sPageName);
		addPagePopup.selectParentPage(sPageNameParent);
		mainPage = addPagePopup.clickOkButton();
		
		Logger.info("Verify point 5: Observe the current page");
		assertTrue(mainPage.isDisplayedChildPage(sPageNameParent, sPageName), 
				"Overview is parent page of page 1");
	}
	
	/*
	 * Verify that user is able to delete sibbling page as long as that page has not children page under it
	 * Dinh Huynh
	 */
	@Test
	public void DA_MP_TC020() {
		Account account = new Account(Constant.USERNAMETA, Constant.PASSWORDTA);
		String sPageName1 = "Page 1";
		String sPageNameParent1 = "Overview";
		
		String sPageName2 = "Page 2";
		String sPageNameParent2 = "Page 1";
		
		String alertWarning = "Cannot delete page \"page 1\" since it has child page(s).";
		
		Logger.info("Step 1: Navigate to Dashboard login page");
		Logger.info("Step 2: Login with valid account");
		mainPage = loginPage.login(account);
		
		Logger.info("Step 3: Go to Global Setting -> Add page");
		mainPage.waitForMainPageLoading();
		addPagePopup = mainPage.openAddpagePopup();
		
		Logger.info("Step 4: Enter info into all required fields on New Page dialog");
		addPagePopup.enterPageName(sPageName1);
		addPagePopup.selectParentPage(sPageNameParent1);
		mainPage = addPagePopup.clickOkButton();
		mainPage.waitForMainPageLoading();
		mainPage.waitForLoading(10);
		
		Logger.info("Step 5: Go to Global Setting -> Add page");
		addPagePopup = mainPage.openAddpagePopup();
		
		Logger.info("Step 6: Enter info into all required fields on New Page dialog");
		addPagePopup.enterPageName(sPageName2);
		addPagePopup.selectParentPage(sPageNameParent2);
		mainPage = addPagePopup.clickOkButton();
		
		Logger.info("Step 7: Go to the first created page: " + sPageName1);
		mainPage.hoverOnPage(sPageNameParent1);
		mainPage.clickOnPageLink(sPageName1);
		
		Logger.info("Step 8: Click Delete link");
		mainPage.clickDeleteLink();
		
		Logger.info("Step 9: Click Ok button on Confirmation Delete page");
		Utilities.acceptAlert();
		
		Logger.info("Verify point 10: Observe the current page");
		assertEquals(Utilities.getTextAlert(), alertWarning,
				"Alert warning displayed");
		
		Logger.info("Step 11: Close confirmation dialog");
		Utilities.dismissAlert();
		
		Logger.info("Step 12: Go to the second page");
		mainPage.hoverOnPage(sPageNameParent1);
		mainPage.hoverOnPage(sPageNameParent2);
		mainPage.clickOnPageLink(sPageName2);
		
		Logger.info("Step 13: Click Delete link");
		mainPage.clickDeleteLink();
		
		Logger.info("Step 14: Click Ok button on Confirmation Delete page");
		mainPage.clickOnPageLink(sPageName2);
		
		Logger.info("Verify point 5: Observe the current page");
		assertFalse(mainPage.isDisplayedChildPage(sPageNameParent2, sPageName2), 
				"Page 2 is delete successfully");
	}
	
	/*
	 * Verify that user is able to edit the name of the page (Parent/Sibbling) successfully
	 * Dinh Huynh
	 */
	@Test
	public void DA_MP_TC021() {
		Account account = new Account(Constant.USERNAMETA, Constant.PASSWORDTA);
		String sPageName1 = "Page 1";
		String sPageNameParent1 = "Overview";
		
		String sPageName2 = "Page 2";
		String sPageNameParent2 = "Page 1";
		
		String sPageName3 = "Page 3";
		String sPageName4 = "Page 4";
		
		Logger.info("Step 1: Navigate to Dashboard login page");
		Logger.info("Step 2: Login with valid account");
		mainPage = loginPage.login(account);
		
		Logger.info("Step 3: Go to Global Setting -> Add page");
		mainPage.waitForMainPageLoading();
		addPagePopup = mainPage.openAddpagePopup();
		
		Logger.info("Step 4: Enter info into all required fields on New Page dialog");
		addPagePopup.enterPageName(sPageName1);
		addPagePopup.selectParentPage(sPageNameParent1);
		mainPage = addPagePopup.clickOkButton();
		mainPage.waitForMainPageLoading();
		mainPage.waitForLoading(10);
		
		Logger.info("Step 5: Go to Global Setting -> Add page");
		addPagePopup = mainPage.openAddpagePopup();
		
		Logger.info("Step 6: Enter info into all required fields on New Page dialog");
		addPagePopup.enterPageName(sPageName2);
		addPagePopup.selectParentPage(sPageNameParent2);
		mainPage = addPagePopup.clickOkButton();
		
		Logger.info("Step 7: Go to the first created page: " + sPageName1);
		mainPage.hoverOnPage(sPageNameParent1);
		mainPage.clickOnPageLink(sPageName1);
		
		Logger.info("Step 8: Click Edit link");
		addPagePopup = mainPage.clickEditLink();
		
		Logger.info("Step 9: Enter another name into Page Name field");
		addPagePopup.enterPageName(sPageName3);
		
		Logger.info("Step 10: Click Ok button on Edit Page dialog");
		mainPage = addPagePopup.clickOkButton();
		
		Logger.info("Verify point 11: Observe the current page");
		assertTrue(mainPage.isDisplayedPageNew(sPageName3), 
				"User is able to edit the name of parent page successfully");
		
		Logger.info("Step 12: Go to the second created page");
		mainPage.hoverOnPage(sPageNameParent1);
		mainPage.hoverOnPage(sPageName3);
		mainPage.clickOnPageLink(sPageName2);
		
		Logger.info("Step 13: Click Edit link");
		addPagePopup = mainPage.clickEditLink();
		
		Logger.info("Step 14: Enter another name into Page Name field");
		addPagePopup.enterPageName(sPageName4);
		
		Logger.info("Step 15: Click Ok button on Edit Page dialog");
		mainPage = addPagePopup.clickOkButton();
		
		Logger.info("Verify point 16: Observe the current page");
		assertTrue(mainPage.isDisplayedChildPage(sPageName3, sPageName4), 
				"User is able to edit the name of sibbling page successfully");
	}
}
