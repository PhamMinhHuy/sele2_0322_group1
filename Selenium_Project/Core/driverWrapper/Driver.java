package driverWrapper;

import java.net.URL;
import java.util.logging.Logger;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;

import common.Constant;
import common.DriverType;

public class Driver extends CoreDriver{
	private static final Logger LOG = Constant.createLogger(Driver.class.getName());
	private static DesiredCapabilities caps;
	private WebDriver _driver;
	
	public Driver( DriverType type, boolean parallel, String hub) {
		try {
			LOG.info("Create new Driver with type: " + type.getValue());
			switch (type.getValue()) {
			case "Chrome" :
				System.setProperty("webdriver.chrome.driver", 
						System.getProperty("user.dir") + "\\Driver\\chromedriver.exe");
				if(parallel) {
					caps = DesiredCapabilities.chrome();
					_driver = new RemoteWebDriver( new URL(hub), caps);
				} else {
					_driver = new ChromeDriver();
				}
				DriverManager.addDriver(_driver);
				break;
				
			case "Firefox":
				System.setProperty("webdriver.chrome.driver", 
						System.getProperty("user.dir") + "\\Driver\\geckodriver.exe");
				if(parallel) {
					caps = DesiredCapabilities.firefox();
					_driver = new RemoteWebDriver( new URL(hub), caps);
				} else {
					_driver = new FirefoxDriver();
				}
				DriverManager.addDriver(_driver);
				break;
			}
		}
		catch(Exception e) {
			LOG.info(e.toString());
		}
	}
	
	public static JavascriptExecutor jsExecutor() {
		return ((JavascriptExecutor) getDriver());
	}
	public static Object executeScript(String script, Object... args) {
		return jsExecutor().executeScript(script);
	}
	public static void scrollTillEnd() {
		LOG.info("Scroll the driver :" + DriverManager.getThreadId() + " till end");
		String js = String.format("window.scrollTo(0, document.body.scrollHeight)");
		jsExecutor().executeScript(js);
	}
}
