package driverWrapper;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;

import org.openqa.selenium.Alert;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import common.Constant;
import common.DriverType;

public class DriverManager {
	private static final Logger LOG = Constant.createLogger(DriverManager.class.getName());
	private static Map<String, WebDriver> map = new HashMap<String, WebDriver>();
	private List<String> windows = new ArrayList<>();

	
//	public static void createDriver(DriverType type) {
//		LOG.info("Thread ID: " + getThreadId());
//		new Driver(type, false, null);
//	}
	public static void createDriver(DriverType type, Boolean remote, String hub) {
		LOG.info("Thread ID: " + getThreadId());
		new Driver(type, remote, hub);
	}
	
	protected static String getThreadId() {
		return String.valueOf(Thread.currentThread().getId());
	}
	
	public static WebDriver getDriver() {
		return map.get(getThreadId());
	}
	
	protected static void addDriver(WebDriver driver) {
		try {
			LOG.info("DriverMangetment are adding driver on thread :" + getThreadId());
			map.put(getThreadId(), driver);
		} catch (Exception e) {
			LOG.severe(e.getMessage());
		}
	}
	
	public static void acceptAlert() {
		try {
			waitForAlertPresent();
			getDriver().switchTo().alert().accept();
		} catch (Exception e) {
			LOG.severe(e.getMessage());
		}
	}
	public static void dismisstAlert() {
		try {
			waitForAlertPresent();
			getDriver().switchTo().alert().dismiss();
		} catch (Exception e) {
			LOG.severe(e.getMessage());
		}
	}
	
	public static String getTexttAlert() {	
		try {
			waitForAlertPresent();
			return getDriver().switchTo().alert().getText();
		} catch (Exception e) {
			LOG.severe(e.getMessage());
			return null;
		}
	}
	public static void waitForAlertPresent() {
        waitForAlertPresent(Constant.LONG_TIMEOUT);
    }

    public static void waitForAlertPresent(long timeOut) {
        try {
            Alert wait = new WebDriverWait(getDriver(), timeOut).until(ExpectedConditions.alertIsPresent());
        } catch (Exception e) {
        	LOG.severe(e.getMessage());
            throw new RuntimeException(e);
        }
    }
	
	public static void switchToFrame(WebElement webElement) {
		try {
			getDriver().switchTo().frame(webElement);
		} catch (Exception e) {
			LOG.severe(e.getMessage());
		}
	}
	


    public void addWindows(String window) {
        if (windows.isEmpty()) {
            windows.add(window);
        }
        if (!windows.isEmpty() && !windows.contains(window)) {
            windows.add(window);
        }
    }
	
	public void switchToWindow(String name) {
        try {
            getDriver().switchTo().window(name);
        } catch (Exception e) {
        	LOG.severe(e.getMessage());
            throw new RuntimeException(e);
        }
    }

    public void switchToFirstWindow() {
        try {
            if (getDriver().getWindowHandle().isEmpty()) {
                addWindows(getDriver().getWindowHandle());
            }
            switchToWindow(windows.get(0));
        } catch (Exception e) {
        	LOG.severe(e.getMessage());
            throw new RuntimeException(e);
        }
    }
    
    public void refresh() {
        getDriver().navigate().refresh();
    }
	
	public static void waitForPageLoad() {
		WebDriverWait wait = new WebDriverWait(getDriver(), Constant.LONG_TIMEOUT);
		wait.until(webDriver -> "complete".equals(((JavascriptExecutor) webDriver).executeScript("return document.readyState")));
	}
}
