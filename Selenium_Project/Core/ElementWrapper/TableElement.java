package ElementWrapper;

import java.util.List;
import java.util.logging.Logger;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import common.Constant;

public class TableElement extends Element{
	
	private static final Logger Logger = Constant.createLogger(Element.class.getName());

	public TableElement(By locator) {
		super(locator);
	}

	public TableElement(String xpath) {
		super(xpath);
	}
	
	public String buildXpathHeader() {
		String str =  String.format("%s//tr[1]//th", this.getLocator().toString());
		return str.replace("By.xpath: ", "");
	}
	
	public String buildXpathByHeaderAndColumn(int index, int column) {
		String str = String.format("%s//tr[%s]//td[%s]//a", this.getLocator().toString(), column, index );
		return str.replace("By.xpath: ", "");
	}

	public String buildTrXpath(int index) {
		String str = String.format("%s//tr//td[%s]", this.getLocator().toString(), index );
		return str.replace("By.xpath: ", "");
	}
	
	public int getIndexOfHeader(String header) {
		try {
			Logger.info(String.format("Get Index of Header on table %s", getLocator().toString()));
			By headerBy = By.xpath(this.buildXpathHeader());
			List<WebElement> EleList = getDriver().findElements(headerBy);
			for (int j = 0; j < EleList.size(); j++) {
				if (EleList.get(j).getText().equals(header)) {
					Logger.info(String.format("Index is %s", j+1));
					return j+1;
				}
			}
			return 0;
			
		} catch (Exception error) {
			Logger.severe(String.format("Has error with control '%s': %s", getLocator().toString(), error.getMessage()));
			throw error;
		}
	}
	
	public int getColumnByHeader(String header, String value) {
		return this.getColumnByIndex(this.getIndexOfHeader(header), value);
	}
	
	public int getColumnByIndex(int index, String value) {
		try {
			Logger.info(String.format("Get Column by Index on table %s", getLocator().toString()));
			List<WebElement> EleList = getDriver().findElements(By.xpath(this.buildTrXpath(index)));
			for (int i = 0; i < EleList.size(); i++) {
				Logger.info(String.format("for i is %s", EleList.get(i).getText()));
				if (EleList.get(i).getText().equals(value)) {
					Logger.info(String.format("Column is %s", i+2));
					return i+2;
				}
			}
			return 0;
		} catch (Exception error) {
			Logger.severe(String.format("Has error with control '%s': %s", getLocator().toString(), error.getMessage()));
			throw error;
		}
	}
	
	public String getValueOfCell(String header, int column) {
		try {
			Logger.info(String.format("Get value of cell on table %s", getLocator().toString()));
			int index = this.getIndexOfHeader(header);
			Element Element = new Element(this.buildXpathByHeaderAndColumn(index, column));
			return Element.getText();
		} catch (Exception error) {
			Logger.severe(String.format("Has error with control '%s': %s", getLocator().toString(), error.getMessage()));
			throw error;
		}
	}
	
	public void clickItem(int index, int column, String text) {
		try {
			String Xpath = this.buildXpathByHeaderAndColumn(index, column);
			List<WebElement> EleList = getDriver().findElements(By.xpath(Xpath));
			for (int i = 0; i < EleList.size(); i++) {
				if (EleList.get(i).getText().equals(text)) {
					Logger.info(String.format("Click Item on table %s", EleList.get(i).getText()));
					EleList.get(i).click();
					break;
				}
			}
		} catch (Exception error) {
			Logger.severe(String.format("Has error with control '%s': %s", getLocator().toString(), error.getMessage()));
			throw error;
		}
	}
}
