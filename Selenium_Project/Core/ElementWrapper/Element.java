package ElementWrapper;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

import org.openqa.selenium.By;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import common.Constant;

public class Element extends BaseElement{

	private static final Logger Logger = Constant.createLogger(Element.class.getName());
	
	public Element(By locator) {
		super(locator);
	}
	
	public Element(String xpath) {
		super(xpath);
	}

	/*=========================Element-Action===========================================================================*/

	public void inputOnVisibleElement(String value, int timeOutInSeconds) {
		if (value != null) {
			this.waitForVisibility(timeOutInSeconds);
			this.sendKeys(value);
		}
	}
	
	public String getVisibleText() {
		this.waitForVisibility(Constant.TIMEOUT);
		return this.getText();
	}
	
	public void selectItemInDropDownList(String txtSelected, int timeInSecond) {
		try {
			this.waitForVisibility();
			if (txtSelected != null) {
				Logger.info(String.format("Select dropdown list with value: '%s'", txtSelected));
				waitForDropDownListPopulated(timeInSecond);
				Select dropDownList = new Select(getElement());
			    for (WebElement option : dropDownList.getOptions()) {
			        if (option.getText().replace("  ", "").equals(txtSelected) && option.isSelected() == false) {
			        	option.click();
			        	break;
			        }
			    }
			}
		} catch (StaleElementReferenceException e){
			selectItemInDropDownList(txtSelected, timeInSecond);
		}
	}
	
	public void clickVisibleElement(int timeOutInSeconds) {
		try {
			this.waitForVisibility(timeOutInSeconds);
			this.click();
		} catch (Exception e) {
			Logger.severe(String.format("Has error with control '%s': %s", getLocator().toString(), e.getMessage()));
			throw e;
		}
	}
	
	public void enter(String sValue) {
		this.waitForClickAble();
		this.sendKeys(sValue);
	}
	public void select(String sValue) {
		Element e = new Element(this.getLocator() + String.format("/option[text()='%s']", sValue));
		e.waitForVisibility();
		this.waitForClickAble();
		this.click();
		e.waitForVisibility();
		e.click();
	}
	public String getLocatorAsString() {
		return getLocator().toString().replace("By.xpath: ", "");
	}
	public boolean isCheck() {
		return isSelected();
	}
	
	public List<String> getAllItemsInDropDownList() {
		List<String> ListOptionsString = new ArrayList<>();
		Select dropDownList = new Select(getElement());
		List<WebElement> ListOptions = dropDownList.getOptions();
		for (int j = 0; j < ListOptions.size(); j++) {
			ListOptionsString.add(ListOptions.get(j).getText());
//			System.out.println(ListOptions.get(j).getText());
		}
		return ListOptionsString;
	}
	/*=========================Element-Waiter===========================================================================*/
	public void waitForElementNotDisplay(int timeOutInSeconds) {
		try {
			Logger.info(String.format("Wait for control %s to be clickabled with timeout: %s", getLocator().toString(), timeOutInSeconds));
			WebDriverWait wait = new WebDriverWait(getDriver(), timeOutInSeconds);
			wait.until(ExpectedConditions.invisibilityOfElementLocated(getLocator()));
		} catch (Exception e) {
			Logger.severe(String.format("Has error with control '%s': %s", getLocator().toString(), e.getMessage()));
			throw e;
		}
	}
	public void waitForElementNotDisplay() {
		waitForElementNotDisplay(Constant.SHORT_TIMEOUT);
	}
	
	public void waitForClickAble(int timeOutInSeconds) {
		try {
			Logger.info(String.format("Wait for control %s to be clickabled with timeout: %s", getLocator().toString(), timeOutInSeconds));
			WebDriverWait wait = new WebDriverWait(getDriver(), timeOutInSeconds);
			wait.until(ExpectedConditions.elementToBeClickable(getLocator()));
		} catch (Exception e) {
			Logger.severe(String.format("Has error with control '%s': %s", getLocator().toString(), e.getMessage()));
			throw e;
		}
	}
	public void waitForClickAble() {
		waitForClickAble(Constant.SHORT_TIMEOUT);
	}
	
	public void waitForPresent() {
		waitForPresent(Constant.TIMEOUT);
	}
	
	public void waitForVisibility() {
		try {
			this.waitForVisibility(Constant.TIMEOUT);
		} catch (Exception error) {
			Logger.severe(String.format("Has error with control '%s': %s", getLocator().toString(), error.getMessage()));
			throw error;
		}
	}
	
	public void waitForVisibility(int timeOutInSeconds) {
		try {
			Logger.info(String.format("Wait for control %s visibility with timeOut: %s", getLocator().toString(),
					timeOutInSeconds));
			WebDriverWait wait = new WebDriverWait(getDriver(), timeOutInSeconds);
			wait.until(ExpectedConditions.visibilityOfElementLocated(getLocator()));
		} catch (Exception error) {
			Logger.severe(String.format("Has error with control '%s': %s", getLocator().toString(), error.getMessage()));
			throw error;
		}
	}

	public void waitForInvisibility(int timeOutInSeconds) {
		try {
			Logger.info(String.format("Wait for control %s invisibility with timeOut: %s", getLocator().toString(),
					timeOutInSeconds));
			WebDriverWait wait = new WebDriverWait(getDriver(), timeOutInSeconds);
			wait.until(ExpectedConditions.invisibilityOfElementLocated(getLocator()));
		} catch (Exception e) {
			Logger.severe(String.format("Has error with control '%s': %s", getLocator().toString(), e.getMessage()));
			throw e;
		}
	}
	
	public boolean returnWaitForVisibility(int timeOutInSeconds) {
		try {
			this.waitForVisibility(timeOutInSeconds);
			return true;
		} catch (Exception e) {
			return false;
		}
	}
	
	public void waitForDropDownListPopulated(int timeOutInSeconds) {
		try {
			Logger.info(String.format("Wait until all options in dropdown list %s are populated", getLocator().toString()));
			WebDriverWait wait = new WebDriverWait(getDriver(), timeOutInSeconds);
			wait.until(ExpectedConditions.refreshed(ExpectedConditions.presenceOfElementLocated(getLocator())));
		} catch (Exception error) {
			Logger.severe(String.format("Has error with control '%s': %s", getLocator().toString(), error.getMessage()));
			throw error;
		}
	}
	
	public boolean returnWaitForInVisibility(int timeOutInSeconds) {
		try {
			this.waitForInvisibility(timeOutInSeconds);
			return true;
		} catch (Exception e) {
			return false;
		}
	}	
//	public void waitForOuterHtmlNotChange(int timeOut) {
//		Logger.info(String.format("Wait until outer html of %s is not change", getLocator().toString()));
//		String currentOuter = this.getAttribute("outerHTML");
//		int time = 0;
//		while (time < timeOut) {
//			try {
//				this.getElement().wait(1000);
//			} catch (InterruptedException e) {
//			}
//			String tmp = this.getAttribute("innterHTML");
//			Logger.info(String.format("innterHTML: %s", tmp));
//			if (tmp.equals(currentOuter)) {
//				break;
//			}
//		currentOuter = tmp; 
//		time += 1;
//		}
//	} 
	
	public void waitForElementNotChange(int timeOut) {
		Logger.info(String.format("Wait until element %s is not change", getLocator().toString()));
		if (this.returnWaitForInVisibility(timeOut)) {
			this.waitForVisibility();
		}
	}
	public String getInnerHTML() {
		return this.getAttribute("innerHTML");
	}
}
