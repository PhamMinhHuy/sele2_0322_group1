package ElementWrapper;

import java.util.List;
import java.util.logging.Logger;

import org.openqa.selenium.By;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.Point;
import org.openqa.selenium.Rectangle;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import common.Constant;
import driverWrapper.DriverManager;

public class BaseElement implements WebElement{

	private static final Logger Logger = Constant.createLogger(BaseElement.class.getName());
	protected WebElement _element = null;
	protected List<WebElement> _elements = null;

	protected WebDriver getDriver() {
		return DriverManager.getDriver();
	}
	
	protected WebElement getElement() {
		try {
			return getDriver().findElement(this.getLocator());
		} catch (Exception e) {
			return null;
		}
	}
	
	protected List<WebElement> getElements(){
		try {
			return getDriver().findElements(this.getLocator());
		} catch (Exception e) {
			return null;
		}
	}
	
	private By _byLocator;
	
	public BaseElement(By locator) {
		this._byLocator = locator;
	}

	public BaseElement(String xpath) {
		this._byLocator = By.xpath(xpath);
	}
	
	public BaseElement(WebElement elem) {
		this._element = elem;
	}
	
	protected By getLocator() {
		return this._byLocator;
	} 
	
	@Deprecated
	public <X> X getScreenshotAs(OutputType<X> target) throws WebDriverException {
		// TODO Auto-generated method stub
		return null;
	}

	public void click() {
		try {
			Logger.info(String.format("Click on %s", this.getLocator().toString()));
			this.getElement().click();
			return;
		} catch (Exception e) {
			Logger.severe(String.format("Has error with control '%s': %s", this.getLocator().toString(), e.getMessage()));
			throw e;
		}
		
	}
	
	protected WebElement waitForPresent(int timeOutInSeconds) {
		Logger.info(String.format("Wait for control %s to be present in DOM with timeOut %s", getLocator().toString(),
				timeOutInSeconds));
		try {
			WebDriverWait wait = new WebDriverWait(getDriver(), timeOutInSeconds);
			_element = wait.until(ExpectedConditions.presenceOfElementLocated(getLocator()));
		} catch (Exception error) {
			Logger.severe(String.format("Has error with control '%s': %s", getLocator().toString(), error.getMessage()));
		}
		return _element;
	}
	
	public void hover() {
		Actions action = new Actions(getDriver());
		action.moveToElement(getElement()).perform();;
	}

	@Deprecated
	public void submit() {
		// TODO Auto-generated method stub
		
	}
	
	public void sendKeys(CharSequence... keysToSend) {
		try {
			Logger.info(String.format("Send keys to element %s", getLocator().toString()));
			this.getElement().clear();
			this.getElement().sendKeys(keysToSend);
		} catch (Exception e) {
			Logger.severe(String.format("Has error with control '%s': %s", getLocator().toString(), e.getMessage()));
			throw e;
		}
	}

	@Deprecated
	public void clear() {
		// TODO Auto-generated method stub
		
	}

	@Deprecated
	public String getTagName() {
		// TODO Auto-generated method stub
		return null;
	}
	
	public String getAttribute(String attributeName) {
		try {
			Logger.info(String.format("Get attribute '%s' of element %s", attributeName, getLocator().toString()));
			return getElement().getAttribute(attributeName);
		} catch (Exception e) {
			Logger.severe(String.format("Has error with control '%s': %s", getLocator().toString(), e.getMessage()));
			throw e;
		}
	}
	
	public boolean isSelected() {
		try {
			return getElement().isSelected();
		} catch (Exception e) {
			Logger.severe(String.format("Has error with control '%s': %s", getLocator().toString(), e.getMessage()));
			return false;
		}
	}

	public boolean isEnabled() {
		try {
			Logger.info(String.format("Check is element '%s' Enabled ", getLocator().toString()));
			return getElement().isEnabled();
		} catch (Exception e) {
			Logger.severe(String.format("Has error with control '%s': %s", getLocator().toString(), e.getMessage()));
			throw e;
		}
	}

	public String getText() {
		try {
			Logger.info(String.format("Get text of element %s", getLocator().toString()));
			return this.getElement().getText();
		} catch (Exception e) {
			Logger.severe(String.format("Has error with control '%s': %s", getLocator().toString(), e.getMessage()));
			throw e;
		}
	}

	@Deprecated
	public List<WebElement> findElements(By by) {
		// TODO Auto-generated method stub
		return null;
	}

	@Deprecated
	public WebElement findElement(By by) {
		// TODO Auto-generated method stub
		return null;
	}

	public boolean isDisplayed() {
		try {
			Logger.info(String.format("Get text of element %s", getLocator().toString()));
			return this.getElement().isDisplayed();
		} catch (Exception e) {
			Logger.severe(String.format("Has error with control '%s': %s", getLocator().toString(), e.getMessage()));
			throw e;
		}
	}

	@Deprecated
	public Point getLocation() {
		// TODO Auto-generated method stub
		return null;
	}

	@Deprecated
	public Dimension getSize() {
		// TODO Auto-generated method stub
		return null;
	}

	@Deprecated
	public Rectangle getRect() {
		// TODO Auto-generated method stub
		return null;
	}

	public String getCssValue(String propertyName) {
		try {
			Logger.info(String.format("Get text of element %s", getLocator().toString()));
			return this.getElement().getCssValue(propertyName);
		} catch (Exception e) {
			Logger.severe(String.format("Has error with control '%s': %s", getLocator().toString(), e.getMessage()));
			throw e;
		}
	}
}
