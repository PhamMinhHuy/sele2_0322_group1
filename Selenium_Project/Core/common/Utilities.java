package common;
import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Random;

import driverWrapper.DriverManager;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class Utilities {

	public static String getProjectPath() {	
		return System.getProperty("user.dir");
	}
	
	public static String  getDateInFuture(int days)
	{
		Calendar calendar = Calendar.getInstance();
		calendar.add(Calendar.DAY_OF_YEAR, days);
		Date futureDateTime = calendar.getTime();
		SimpleDateFormat dateFormat = new SimpleDateFormat("M/d/yyyy");
		String futureDate = dateFormat.format(futureDateTime).toString();
		return futureDate;
	}
	
	public static String getActivationCode(String string) {
		String separator =": ";
		int sepPos = string.indexOf(separator);
		if (sepPos == -1) {
	         System.out.println("");
	   }
	return string.substring(sepPos + separator.length());
	}
	
	public static String generateRandomEmail(int length) {
		String alphabet = "abcdefghijklmnopqrstuvwyzABCDEFGHIJKLMNOPQRSTUVWXYZ123456789";
		StringBuilder sb = new StringBuilder();
		Random random = new Random();
		
		for(int i = 0; i < length; i++) {
		   int index = random.nextInt(alphabet.length());
		   char randomChar = alphabet.charAt(index);
		   sb.append(randomChar);
		}

		
		String randomString = sb.toString();
		return randomString + generateRandomDomain(10);
	}
	
	public static String generateRandomDomain(int length) {
		String alphabet = "abcdefghijklmnopqrstuvwyz1234567890";
		StringBuilder sb = new StringBuilder();
		Random random = new Random();		
		for(int i = 0; i < length; i++) {
			   int index = random.nextInt(alphabet.length());
			   char randomChar = alphabet.charAt(index);
			   sb.append(randomChar);
			}
		String randomString = sb.toString();
		
		String[] domains = {"de","com","in","en","us","guru","vn","lnk","ml"};
		int idx = new Random().nextInt(domains.length);
        String domain = (domains[idx]).toString();
        
		return "@"+randomString+"."+domain;
	}
		
	public static String getSubString(String string) {
		String subString = string.substring( 0, string.indexOf("."));
		return subString;
	}

	public static boolean checkMessageExist(String value)
	{
		WebDriverWait wait = new WebDriverWait(DriverManager.getDriver(), Constant.TIMEOUT);
		wait.until(ExpectedConditions.alertIsPresent());
		String msg = DriverManager.getTexttAlert();
		System.out.println("The text on MessageBox is " + msg);
		DriverManager.acceptAlert();
		return msg.equals(value);
	}
	
	public static void acceptAlert() {
		WebDriverWait wait = new WebDriverWait(DriverManager.getDriver(), Constant.TIMEOUT);
		wait.until(ExpectedConditions.alertIsPresent());
		DriverManager.acceptAlert();
		Utilities.waitForPageLoading();
	}
	
    public static boolean isAlpha(String s) {
        return s != null && s.matches("^[a-zA-Z]*$");
    }
    
    public static List<String> sortListItem(List<String> list) {
        Collections.sort(list);
        return list;
    }
    
	public static String takeScreenShot(String filename, String filepath) throws Exception {
		String path = "";
		try {
			// Convert web driver object to TakeScreenshot
			TakesScreenshot scrShot = ((TakesScreenshot) DriverManager.getDriver());

			// Call getScreenshotAs method to create image file
			File SrcFile = scrShot.getScreenshotAs(OutputType.FILE);

			// Move image file to new destination
			File DestFile = new File(filepath + File.separator + filename + ".png");

			// Copy file at destination
			FileUtils.copyFile(SrcFile, DestFile);
			path = DestFile.getAbsolutePath();
		} catch (Exception e) {
			System.err.println("An error occurred when capturing screen shot: " + e.getMessage());
		}
		return path;
	}
	
	public static String getDateNow(String format) {
		SimpleDateFormat formatter = new SimpleDateFormat(format);
	    Date date = new Date();
	    return formatter.format(date);
	}
	
	public static void waitForPageLoading() {
		DriverManager.waitForPageLoad();
	}
	public static int randomInt(int min, int max) {
        Random rand = new Random();
        return min + rand.nextInt((max - min) + 1);
    }
	
	public static boolean compareLists(List<String> actual, List<String> expected) {
        return actual.size() == expected.size() && actual.containsAll(expected);
    }
	
	public static String getTextAlert() {
		waitForPageLoading();
		return DriverManager.getTexttAlert();
	}
	
	public static void dismissAlert() {
		waitForPageLoading();
		DriverManager.dismisstAlert();
	}
}

	
