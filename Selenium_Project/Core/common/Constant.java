package common;

import java.util.logging.Logger;

public class Constant {
	public static final int TIMEOUT = 10;
	public static final int SHORT_TIMEOUT = 5;
	public static final int LONG_TIMEOUT = 60;
	public static final String RAILWAY_URL = "http://b151-123-30-67-33.ngrok.io/TADashboard/login.jsp";
	public static final String RAILWAY_URL2 = "http://a896-123-30-67-33.ngrok.io/TADashboard/login.jsp";
	public static final String USERNAMETA = "administrator";
	public static final String PASSWORDTA = "";
	public static final String USERNAMETA1 = "test";
	public static final String USERNAMETA1_UPPERCASE = "TEST";
	public static final String PASSWORDTA1_UPPERCASE = "TEST";
	public static final String PASSWORDTA1_LOWSERCASE = "test";
	public static final String INVALID_USERNAMETA = "abc";
	public static final String INVALID_PASSWORDTA = "abc";
	public static final String REPOSITORY_NAME_1 = "SampleRepository";
	public static final String REPOSITORY_NAME_2 = "SampleRepository_2";
	public static final String ERROR_MESSAGE = "Username or password is invalid";
	public static final String ERROR_MESSAGE_1 = "Display Name is a required field.";
	public static final String ERROR_MESSAGE_TC010 = "Please enter username";
	public static final String USERNAMETA_TC008 = "tc008";
	public static final String PASSWORDTA_TC008 = "p@ssw0rd";
	public static final String USERNAMETA_TC009 = "tc009!";
	public static final String PASSWORDTA_TC009 = "taadmin";
	public static final String DISPLAY_NAME = "Logigear";
	public static final String DISPLAY_NAME_TC030 = "Logigear#$%";
	public static final String DISPLAY_NAME_TC032 = "Duplicated panel";
	public static final String DISPLAY_NAME_TC033 = "giang - panel";
	public static final String PROFILES_NAME_TC034 = "giang - data";
	public static final String CHART_TITLE_TC035 = "Chart#$%";
	public static final String CHART_TITLE_TC035_2 = "Chart@";
	public static final String DISPLAY_NAME2_TC030 = "Logigear@";
	public static final String ERROR_MESSAGE_TC_030 = "Invalid display name. The name cannot contain high ASCII characters or any of the following characters: /:*?<>|\\\"#[]{}=%;";
	public static final String NULL = null;
	public static final String ERROR_MESSAGE_TC_032 = "Dupicated panel already exists. Please enter a different name.";
	public static final String Name = "Name";
	public static final String ERROR_MESSAGE_TC_035 = "Invalid title name. The name cannot contain high ASCII characters or any of the following characters: /:*?<>|\\\"#[]{}=%;";

	static {
		String path = System.getProperty("user.dir") + "\\Core\\common\\logging.properties";
		System.setProperty("java.util.logging.config.file" , path);
	}
	
	public static final Logger createLogger(String className) {
		return Logger.getLogger(className);
	}
}
