package common;

public enum DriverType {
	Chrome("Chrome"),
	FireFox("FireFox");
	
	private final String value;

	DriverType(String value) {
		this.value = value;
	}
	
	public String getValue() {
		return this.value;
	}
}
